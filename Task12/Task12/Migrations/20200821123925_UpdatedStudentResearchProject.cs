﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Task12.Migrations
{
    public partial class UpdatedStudentResearchProject : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "ResearchProjectId",
                table: "Student",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.CreateIndex(
                name: "IX_Student_ResearchProjectId",
                table: "Student",
                column: "ResearchProjectId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Student_ResearchProject_ResearchProjectId",
                table: "Student",
                column: "ResearchProjectId",
                principalTable: "ResearchProject",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Student_ResearchProject_ResearchProjectId",
                table: "Student");

            migrationBuilder.DropIndex(
                name: "IX_Student_ResearchProjectId",
                table: "Student");

            migrationBuilder.DropColumn(
                name: "ResearchProjectId",
                table: "Student");
        }
    }
}
