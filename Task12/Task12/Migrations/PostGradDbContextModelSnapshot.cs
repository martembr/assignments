﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using Task12.Models;

namespace Task12.Migrations
{
    [DbContext(typeof(PostGradDbContext))]
    partial class PostGradDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.7")
                .HasAnnotation("Relational:MaxIdentifierLength", 128)
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("Task12.Models.Professor", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<string>("ResearchField")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("Professor");
                });

            modelBuilder.Entity("Task12.Models.ResearchProject", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.HasKey("Id");

                    b.ToTable("ResearchProject");
                });

            modelBuilder.Entity("Task12.Models.ResearchProjectProfessor", b =>
                {
                    b.Property<int>("ProfessorId")
                        .HasColumnType("int");

                    b.Property<int>("ResarchId")
                        .HasColumnType("int");

                    b.Property<int?>("ResearchProjectId")
                        .HasColumnType("int");

                    b.HasKey("ProfessorId", "ResarchId");

                    b.HasIndex("ResearchProjectId");

                    b.ToTable("ResearchProjectProfessor");
                });

            modelBuilder.Entity("Task12.Models.Student", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("int")
                        .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

                    b.Property<string>("Name")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("ProfessorId")
                        .HasColumnType("int");

                    b.Property<string>("Program")
                        .HasColumnType("nvarchar(max)");

                    b.Property<int>("ResearchProjectId")
                        .HasColumnType("int");

                    b.HasKey("Id");

                    b.HasIndex("ProfessorId");

                    b.HasIndex("ResearchProjectId")
                        .IsUnique();

                    b.ToTable("Student");
                });

            modelBuilder.Entity("Task12.Models.ResearchProjectProfessor", b =>
                {
                    b.HasOne("Task12.Models.Professor", "Professor")
                        .WithMany("ResearchProjectProfessors")
                        .HasForeignKey("ProfessorId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Task12.Models.ResearchProject", "ResearchProject")
                        .WithMany("ResearchProjectProfessors")
                        .HasForeignKey("ResearchProjectId");
                });

            modelBuilder.Entity("Task12.Models.Student", b =>
                {
                    b.HasOne("Task12.Models.Professor", "Professor")
                        .WithMany("Students")
                        .HasForeignKey("ProfessorId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();

                    b.HasOne("Task12.Models.ResearchProject", "ResearchProject")
                        .WithOne("Student")
                        .HasForeignKey("Task12.Models.Student", "ResearchProjectId")
                        .OnDelete(DeleteBehavior.Cascade)
                        .IsRequired();
                });
#pragma warning restore 612, 618
        }
    }
}
