﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Task12.Migrations
{
    public partial class ResearchProjectProfessor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "ResearchProject",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ResearchProject", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "ResearchProjectProfessor",
                columns: table => new
                {
                    ProfessorId = table.Column<int>(nullable: false),
                    ResarchId = table.Column<int>(nullable: false),
                    ResearchProjectId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ResearchProjectProfessor", x => new { x.ProfessorId, x.ResarchId });
                    table.ForeignKey(
                        name: "FK_ResearchProjectProfessor_Professor_ProfessorId",
                        column: x => x.ProfessorId,
                        principalTable: "Professor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_ResearchProjectProfessor_ResearchProject_ResearchProjectId",
                        column: x => x.ResearchProjectId,
                        principalTable: "ResearchProject",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ResearchProjectProfessor_ResearchProjectId",
                table: "ResearchProjectProfessor",
                column: "ResearchProjectId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ResearchProjectProfessor");

            migrationBuilder.DropTable(
                name: "ResearchProject");
        }
    }
}
