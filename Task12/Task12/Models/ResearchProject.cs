﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task12.Models
{
    public class ResearchProject
    {
        // Primary key - id
        public int Id { get; set; }

        // other properties
        public string Name { get; set; }

        // relationship - many professors can be attached to a research project
        // only one student can be attached to the project 

        // navigation

        // Collection of ResearchProjectProfessors is a navigation property
        // for connection ResearchProject to Professor
        public ICollection<ResearchProjectProfessor> ResearchProjectProfessors { get; set; }
 
        // Student-object is a navigation property
        public Student Student { get; set; }

    }
}
