﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task12.Models
{
    public class Professor
    {
        // Primary key
        public int Id { get; set; }

        public string Name { get; set; }
        public string ResearchField { get; set; }


        // relationships
        // a professor can have many students that they supervise
        // and they can work on many research projects

        // navigation
        // Collection of Students is a navigation property
        // for connecting to students
        public ICollection<Student> Students { get; set; }

        // Collection of ResearchProjectProfessors is a navigation property
        // for connection ResearchProject to Professor
        public ICollection<ResearchProjectProfessor> ResearchProjectProfessors { get; set; }

        public override string ToString()
        {
            return $"Professor: {Name} id: {Id}";
        }
    }
}
