﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task12.Models
{
    public class Student
    {
        // Primary key
        public int Id { get; set; }

        // properties
        public string Name { get; set; }
        public string Program { get; set; }

        // relationships - a student has one professor as a supervisor
        // and a student can be part of one research project

        // navigation and FKs 
        // ProfessorId is a FK to Professor and the Professor-object
        // is a navigation property
        public int ProfessorId {get ; set; }
        public Professor Professor { get; set; }
        // ResarchProjectId is a FK to ResearchProject and the 
        // ResearchProject-object is a navigation property
        public int ResearchProjectId { get; set; }
        public ResearchProject ResearchProject { get; set; }


        public override string ToString()
        {
            return $"Student: {Name} id: {Id}";
        }
    }
}
