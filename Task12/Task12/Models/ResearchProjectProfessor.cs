﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task12.Models
{
    public class ResearchProjectProfessor
    {
        // linking professors and research projects

        // Combination of ProfessorId and ResearchId is the Primary Key

        public int ProfessorId { get; set; }
        public int ResarchId { get; set; }


        // navigation properties:
        public Professor Professor { get; set; }

        public ResearchProject ResearchProject { get; set; }
    }
}
