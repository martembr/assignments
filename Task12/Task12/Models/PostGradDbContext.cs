﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Task12.Models
{
    public class PostGradDbContext :DbContext
    {
        public DbSet<Professor> Professor { get; set; }
        public DbSet<Student> Student { get; set; }
        public DbSet<ResearchProject> ResearchProject { get; set; }
        public DbSet<ResearchProjectProfessor> ResearchProjectProfessor { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source =PC7389\\SQLEXPRESS;Initial Catalog=PostGradDB;Integrated Security=True;");
        }

        // set key for ResearchProjectProfessor as combination of id's
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ResearchProjectProfessor>().HasKey(rpp => new {rpp.ProfessorId, rpp.ResarchId });
        }

    }
}
