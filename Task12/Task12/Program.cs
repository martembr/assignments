﻿using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using Task12.Models;
using System.IO;
using System.Text;

namespace Task12
{
    class Program
    {


        static void Main(string[] args)
        {

            // Demonstrating the methods:
            // AddSomething();


            /*

            // get out list of students and professors
            ICollection<Student> students = GetStudentList();
            ICollection<Professor> professors = GetProfessorList();

            // print info on students in console
            foreach(Student stud in students)
            {
                Console.WriteLine(stud);
            }

            // write professors to json file
            foreach (Professor prof in professors)
            {
                SerializeToJson<Professor>(prof, "professors.json");
            }
            */
            Menu();

        }

        // menu to allow user to choose some of the operations
        static void Menu()
        {
            int userChoice = -1;
            string menuText = "Please choose an option: \n"
                + "1 - view all students \n"
                + "2 - view all professors \n"
                + "3 - update student \n"
                + "4 - write info on professors to json file \n"
                + "0 - exit";
            while (userChoice != 0)
            {
                userChoice = GetMenuChoice(menuText, 0, 4);
                ICollection<Student> students;
                ICollection<Professor> professors;
                switch (userChoice) {
                    case 1:
                        students = GetStudentList();
                        // print info on students in console
                        foreach (Student stud in students)
                        {
                            Console.WriteLine(stud);
                        }
                        break;
                    case 2:
                        professors = GetProfessorList();

                        // print info on students in console
                        foreach (Professor prof in professors)
                        {
                            Console.WriteLine(prof);
                        }
                        break;
                    case 3:
                        Console.WriteLine("Students in the system: ");
                        students = GetStudentList();
                        // print info on students in console
                        foreach (Student stud in students)
                        {
                            Console.WriteLine(stud);
                        }
                        int studId = GetMenuChoice("Choose an id: ", 0, 1000);
                        Student studToUpdate = GetStudent(studId);
                        Console.WriteLine("Enter new name");
                        string newName = Console.ReadLine();
                        UpdateStudentName(studToUpdate, newName);
                        break;
                    case 4:
                        Console.WriteLine("Please enter name of file: ");
                        string fileName = Console.ReadLine();
                        professors = GetProfessorList();

                        // print info on students in console
                        foreach (Professor prof in professors)
                        {
                            SerializeToJson<Professor>(prof, fileName);
                        }
                        break;
                    case 0:
                        Console.WriteLine("Goodbye");
                        break;

                }
            }
        }


        // gets an int from user between lowerbound and upperbound
        public static int GetMenuChoice(string msg, int lowerbound, int upperbound)
        {
            int choice = lowerbound - 1;
            while (choice < lowerbound || choice > upperbound)
            {
                Console.WriteLine(msg);
                try
                {
                    choice = Convert.ToInt32(Console.ReadLine());
                }
                catch (Exception ex)
                {
                    //Console.WriteLine(ex.Message);
                    Console.WriteLine($"Please type an integer between {lowerbound} and {upperbound}");
                }
            }
            return choice;
        }

        // take in object, create json string and writes to file
        static void SerializeToJson<T>(T objectToSerialize, string fileName)
        {
            String jsonString = JsonConvert.SerializeObject(objectToSerialize, Formatting.Indented,
                new JsonSerializerSettings() { ReferenceLoopHandling = ReferenceLoopHandling.Ignore });
            //Console.WriteLine(jsonString);
            try
            {
                // create file if it doesn't exists
                if (!File.Exists(fileName))
                {
                    Console.WriteLine("creating file");
                    File.WriteAllText(fileName, jsonString);

                }
                else
                {
                    // append text
                    File.AppendAllText(fileName, jsonString);
                }
            } catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        // method used for filling in sample objects
        static void AddSomething()
        {
            try
            {
                Professor profHansen = CreateProfessor("Prof. Hansen");
                ResearchProject rproj = CreateResearchProject("Some project");
                Student hans = CreateStudent("Hans", "Physics", profHansen.Id, rproj.Id);

                Professor profJensen = CreateProfessor("Prof. Jensen", "Topology");
                ResearchProject rproj2 = CreateResearchProject("Some other project");
                Student per = CreateStudent("Per", "Mathematics", profJensen.Id, rproj2.Id);

                ResearchProject rproj3 = CreateResearchProject("Really cool project");
                Student peter = CreateStudent("Peter", "Physics", GetProfessor("Prof. Hansen").Id, rproj3.Id);

                UpdateProfessorResearchField(GetProfessor("Prof. Hansen"), "String theory");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }


        // CRUD operations for students and professors


        // Creating research project
        static ResearchProject CreateResearchProject(string name)
        {
            ResearchProject newResearchProject = new ResearchProject() { Name = name };
            using (PostGradDbContext pgContext = new PostGradDbContext())
            {
                pgContext.ResearchProject.Add(newResearchProject);
                pgContext.SaveChanges();
            }
            return newResearchProject;
        }

        // Creating students
        static Student CreateStudent(string name, string program, int profid, int rpid) {
            Student newStudent = new Student() { Name = name, Program = program, ProfessorId = profid, ResearchProjectId = rpid };
            using (PostGradDbContext pgContext = new PostGradDbContext())
            { 
                pgContext.Student.Add(newStudent);
                pgContext.SaveChanges();
            }
            return newStudent;
        }

        // Creating professors
        static Professor CreateProfessor(string name) 
        {
            Professor newProfessor = new Professor() { Name = name };
            using (PostGradDbContext pgContext = new PostGradDbContext())
            {
                pgContext.Professor.Add(newProfessor);
                pgContext.SaveChanges();
            }
            return newProfessor;
        }
        static Professor CreateProfessor(string name, string researchfield) 
        {
            Professor newProfessor = new Professor() { Name = name, ResearchField = researchfield };
            using (PostGradDbContext pgContext = new PostGradDbContext())
            {
                pgContext.Professor.Add(newProfessor);
                pgContext.SaveChanges();
            }
            return newProfessor;
        }


        // gettting all the students
        static ICollection<Student> GetStudentList()
        {
            ICollection<Student> allStudents;
            using (PostGradDbContext pgContext = new PostGradDbContext())
            { 
                allStudents = pgContext.Student.ToList();
            }
            return allStudents;
        }

        // gettting all the professors
        static ICollection<Professor> GetProfessorList()
        {
            ICollection<Professor> allProfs;
            using (PostGradDbContext pgContext = new PostGradDbContext())
            {
                allProfs = pgContext.Professor.ToList();
            }
            return allProfs;
        }

        // returns student with matching id
        static Student GetStudent(int studentid)
        {
            Student returnStudent;
            using (PostGradDbContext pgContext = new PostGradDbContext())
            {
                returnStudent =  pgContext.Student.Where(s => s.Id == studentid).Single();
            }
            return returnStudent;
        }

        // returns student with matching name
        static Student GetStudent(string studentName)
        {
            Student returnStudent;
            using (PostGradDbContext pgContext = new PostGradDbContext())
            {
                returnStudent = pgContext.Student.Where(s => s.Name == studentName).Single();
            }
            return returnStudent;
        }

        // get professor with given name
        static Professor GetProfessor(string profName)
        {
            Professor returnProf;
            using (PostGradDbContext pgContext = new PostGradDbContext())
            {
                returnProf = pgContext.Professor.Where(p => p.Name == profName).Single();
            }
            return returnProf;
        }

        // changes name of given student
        static void UpdateStudentName(Student student, string newName)
        {
            using (PostGradDbContext pgContext = new PostGradDbContext())
            {
                student.Name = newName;
                pgContext.SaveChanges();
            }
        }


        // changes program of given student
        static void UpdateStudentProgram(Student student, string newProgram)
        {
            using (PostGradDbContext pgContext = new PostGradDbContext())
            {
                student.Program = newProgram;
                pgContext.SaveChanges();
            }
        }

        // link student to professor
        static void UpdateStudentSupervisor(Student student, int profid)
        {
            using (PostGradDbContext pgContext = new PostGradDbContext())
            {
                student.ProfessorId = profid;
                pgContext.SaveChanges();
            }
        }

        // change a professors research field
        static void UpdateProfessorResearchField(Professor prof, string newResearchField)
        {
            using (PostGradDbContext pgContext = new PostGradDbContext())
            {
                prof.ResearchField = newResearchField;
                pgContext.SaveChanges();
            }
        }


        // delete student 
        static void DeleteStudent(Student student)
        {
            using (PostGradDbContext pgContext = new PostGradDbContext())
            {
                pgContext.Student.Remove(student);
                pgContext.SaveChanges();
            }
        }

        // delete professor
        static void DeleteProfessor(Professor prof) 
        {
            using (PostGradDbContext pgContext = new PostGradDbContext())
            {
                pgContext.Professor.Remove(prof);
                pgContext.SaveChanges();
            }
        }
    }
}
