﻿using System;
using System.Runtime.ExceptionServices;

namespace FirstApp
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to this app!");
            Console.WriteLine("My name is Marte");
            Console.WriteLine("What is your name? ");
            string name = Console.ReadLine();
            int len = name.Length;
            char first = ' ';
            if (len != 0)
            {
                first = name[0];
            }
            Console.WriteLine($"Hello {name} your name is {len} characters long and starts with {first}");
        }
    }
}
