﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task10
{
    interface ITrainable
    {
        public void Train(String task);
    }
}
