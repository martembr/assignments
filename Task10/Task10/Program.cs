﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task10
{
    class Program
    {

        static List<Animal> animals = new List<Animal>();

        static void Main(string[] args)
        {
            Menu();
        }


        // Starting menu
        static void Menu()
        {
            Console.WriteLine("Choose between: \na - add animals \nb - use existing list");
            if (Console.ReadLine() == "a")
            {
                string choice = "y"; 
                while (choice == "y")
                {
                    AddAnimal();
                    Console.WriteLine("Do you wish to add more animals? y/n ");
                    choice = Console.ReadLine();
                }
            } else
            {
                InitializeAnimalsList();
            }
            Console.WriteLine("The animals: ");
            foreach (Animal ani in animals)
            {
                Console.WriteLine(ani);
            }
            FilterMenu();
        }


        // Menu for filtering animals
        static void FilterMenu()
        {
            string menuText = "\nChoose option: \n"
                + "1 - filter by age \n"
                + "2 - filter ny name \n"
                + "3 - filter by trainable \n"
                + "e - exit";
            string choice = "";
            while (choice != "e")
            {
                Console.WriteLine(menuText);
                choice = Console.ReadLine();
                switch (choice)
                {
                    case "1":
                        int maximumAge = TakeInNumber("Please enter maximum age: ");
                        Console.WriteLine($"\nAnimals under the age of {maximumAge}");
                        PrintList(FilterByAge(maximumAge));
                        break;
                    case "2":
                        Console.WriteLine("Enter the name to filter by: ");
                        String name = Console.ReadLine();
                        Console.WriteLine($"\nAnimals with names containing {name}");        
                        PrintList(FilterByName(name));
                        break;
                    case "3":
                        Console.WriteLine("\nTrainable animals");
                        PrintList(FilterByTrainable());
                        break;
                    case "e":
                        Console.WriteLine("Goodbye!");
                        break;
                    default:
                        Console.WriteLine("\nUnknown option. Please try again\n");
                        break;
                }
            }
        }

        // Takes in and returns a postive integer from the user 
        static int TakeInNumber(String msg)
        {
            int number = -1;
            while (number < 0) {
                Console.WriteLine(msg);
                try
                {
                    number = Convert.ToInt32(Console.ReadLine());
                }
                catch (Exception e) { }
            }
            return number;
        }

        // Filters out and returns all the animals with an age less than
        // or equal to given age
        static IEnumerable<Animal> FilterByAge(int age)
        {
            IEnumerable<Animal> result =
                (from animal in animals
                 where animal.Age <= age
                 select animal);
            return result;
        }

        // Filters out and returns all the animals with names containing given name
        static IEnumerable<Animal> FilterByName(string name)
        {
            IEnumerable<Animal> result =
                (from animal in animals
                 where animal.Name.Contains(name)
                 select animal);
            return result;
        }

        // Filters out and returns all the trainable animals
        static IEnumerable<Animal> FilterByTrainable()
        {
            IEnumerable<Animal> result =
                (from animal in animals
                 where animal is ITrainable
                 select animal);
            return result;
        }

        // Prints an IEnumerable list of animals
        static void PrintList(IEnumerable<Animal> filteredAnimals)
        {
            Console.WriteLine("Found {0} animals ", filteredAnimals.Count());
            foreach (Animal ani in filteredAnimals) {
                Console.WriteLine(ani);
            }
        }


        // Fills list with some animals 
        static void InitializeAnimalsList()
        {
            animals.Add(new Poodle("Fido", 7, "football"));
            animals.Add(new GoldenRetriever("Hans", 5, "tennis ball"));
            animals.Add(new GoldenRetriever("Per", 5, "stuffed teddy bear"));
            animals.Add(new Poodle("Fia", 4, "stuffed rat"));
            animals.Add(new Cat("Mao", 6));
            animals.Add(new Cat("Mons", 3));
        }

        static void AddAnimal()
        {
            string msg = "Please choose what animal to add: \n"
                + "1 - Cat\n"
                + "2 - Poodle\n"
                + "3 - Golden Retriever \n";
            string choice = "";
            while (choice != "1" && choice != "2" && choice != "3")
            {
                Console.WriteLine(msg);
                choice = Console.ReadLine();
            }
            Console.WriteLine("Please enter the name: ");
            string name = Console.ReadLine();
            int age = TakeInNumber("Please enter the age: "); 
            if (choice != "1")
            {
                Console.WriteLine("Please enter favorite toy: ");
                string toy = Console.ReadLine();
                if (choice == "2")
                {
                    animals.Add(new Poodle(name, age, toy));
                } else
                {
                    animals.Add(new GoldenRetriever(name, age, toy));
                }
            } else
            {
                animals.Add(new Cat(name, age));
            }
            Console.WriteLine("Animal added!");
        }




    }
}
