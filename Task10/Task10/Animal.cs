﻿namespace Task10
{
    public abstract class Animal
    {

        public string Name { get; set; }
        public int Age { get; set; }

        public Animal(string name, int age)
        {
            Name = name;
            Age = age;
        }

        public Animal() { }

        public abstract void MakeSound();
        public override string ToString()
        {
            return $"{Name}, {Age}";
        }

    }
}
