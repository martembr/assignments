﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task10
{

    public class Cat : Animal, IClimber, IRunning
    {

        public Cat() : base() { }
        public Cat(string name, int age) : base(name, age) { }

        public void Climb()
        {
            Console.WriteLine("Climbing up a tree looking for squirrels");
        }

        public override void MakeSound()
        {
            Console.WriteLine("meow!");
        }

        public void Move()
        {
            Console.WriteLine("Elegantly runs from one hiding place to the next");
        }
    }

}
