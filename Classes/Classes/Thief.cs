﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Classes
{
    public class Thief : Character
    {
        public Thief() : base()
        {
            ArmorRating = 5;
        }


        public override void Move()
        {
            Console.WriteLine("Sneaking around");
        }

        public override void Attack()
        {
            Console.WriteLine("Stabs the enemy in the back");
        }
    }
}
