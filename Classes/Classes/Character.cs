﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Classes
{
    public abstract class Character : IAttacking, IMoving
    {
        // properties 
        public int HP { get; set; }
        public int Energy { get; set; }
        public string Name { get; set; }
        public int ArmorRating { get; set; }

        public Character()
        {
            HP = 100;
            Energy = 100;
        }


        // default behavoir for attack
        public virtual void Attack()
        {
            Console.WriteLine("Attacking");
        }

        public abstract void Move();

        public override string ToString()
        {
            return $"{Name}, Armor rating: {ArmorRating}, HP: {HP}, Energy: {Energy}";
        }
    }
}
