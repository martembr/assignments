﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Classes
{
    public class Warrior : Character
    {

        public Warrior() : base()
        {
            ArmorRating = 50;  
        }

        public override void Move()
        {
            Console.WriteLine("Runs towards danger");
        }
    }
}
