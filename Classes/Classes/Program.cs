﻿using System;
using System.Collections.Generic;

namespace Classes
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Character> characters = new List<Character>();
            characters.Add(new Thief() { Name = "Thief1" });
            characters.Add(new Thief() { Name = "Thief2" });
            characters.Add(new Warrior() { Name = "Warrior1" });
            characters.Add(new Warrior() { Name = "Warrior2" });
            characters.Add(new Wizard() { Name = "Wizard1" });
            characters.Add(new Wizard() { Name = "Wizard2" });

            foreach (Character character in characters) {
                Console.WriteLine(character);
            }
        }
    }
}
