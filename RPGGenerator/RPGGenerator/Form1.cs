﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using RPGClassLibrary;

namespace RPGGenerator
{
    public partial class Form1 : Form
    {

        private Dictionary<int, string> charTypes = null;
        SqlConnectionStringBuilder builder = null;

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // initialize builder 
            builder = new SqlConnectionStringBuilder();
            builder.DataSource = "PC7389\\SQLEXPRESS";
            builder.InitialCatalog = "RPGCharactersDB";
            builder.IntegratedSecurity = true;

            // create dictionary for combobox display
            charTypes = new Dictionary<int, string>();
            charTypes.Add(0, "Wizard");
            charTypes.Add(1, "Warrior");
            charTypes.Add(2, "Thief");
            // set up combobox with appropriate options
            cbCharacterSelection.ValueMember = "Key";
            cbCharacterSelection.DisplayMember = "Value";
            cbCharacterSelection.DataSource = new BindingSource(charTypes, null);


            // read in characters from database and set up listbox to display characters 
            lbCharacterDisplay.BeginUpdate();
            ReadAllCharactersFromDB();
            lbCharacterDisplay.EndUpdate();

        }

        // reads in all the characters from the database, creates a character object
        // and adds it to the listbox
        private void ReadAllCharactersFromDB()
        {
            string sql = "SELECT * FROM Character";
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        using (SqlDataReader reader = command.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                // take in properties 
                                int ID = reader.GetInt32(0);
                                string chartype = reader.GetString(1);
                                string name = reader.GetString(2);
                                int HP = reader.GetInt32(3);
                                int energy  = reader.GetInt32(4);
                                int armorRating = reader.GetInt32(5);


                                // create Character and add to listbox
                                RPGCharacter newCharacter = CreateCharacter(chartype, name, HP, energy, armorRating);
                                newCharacter.ID = ID;
                                // add to listbox
                                lbCharacterDisplay.Items.Add(newCharacter);
                            }
                        }

                    }


                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        // function that is called when the create button is clicked
        private void btnCreate_Click(object sender, EventArgs e)
        {
            // check if name is entered and display message if not
            if (tbEnterName.Text == "")
            {
                MessageBox.Show("Please enter a name!");
                return;
            }

            // create new character
            RPGCharacter newCharacter = CreateCharacter();
           
            // show character info in message box and add it to the list
            MessageBox.Show("Created new character!\n" + newCharacter.ToString());
      
            lbCharacterDisplay.BeginUpdate();
            lbCharacterDisplay.Items.Add(newCharacter);
            lbCharacterDisplay.EndUpdate();

            // reset name field
            tbEnterName.Text = "";

            // store to file
           // AddCharacterToFile(newCharacter, @"characterInfo.txt");

            // add to database 
            AddCharacterToDB(newCharacter);
        }


        // creates and returns a new RPGCharacter
        public RPGCharacter CreateCharacter()
        {
            int typeOfCharacter = cbCharacterSelection.SelectedIndex;
            RPGCharacter newCharacter = null;
            switch (typeOfCharacter)
            {
                case 0:
                    // wizard
                    newCharacter = new Wizard() { Name = tbEnterName.Text };
                    break;
                case 1:
                    // warrior
                    newCharacter = new Warrior() { Name = tbEnterName.Text };
                    break;
                case 2:
                    // thief 
                    newCharacter = new Thief() { Name = tbEnterName.Text };
                    break;
            }
            return newCharacter;
        }

        // creates and returns a new RPGCharacter with given properties
        public RPGCharacter CreateCharacter(string typeOfCharacter, string name, int hp, int energy, int armorRating)
        {
            RPGCharacter newCharacter = null;
            switch (typeOfCharacter)
            {
                case "Wizard":
                    // wizard
                    newCharacter = new Wizard() { Name = name, HP = hp, Energy = energy, ArmorRating = armorRating };
                    break;
                case "Warrior":
                    // warrior
                    newCharacter = new Warrior() { Name = name, HP = hp, Energy = energy, ArmorRating = armorRating };
                    break;
                case "Thief":
                    // thief 
                    newCharacter = new Thief() { Name = name, HP = hp, Energy = energy, ArmorRating = armorRating };
                    break;
            }
            return newCharacter;
        }

        // write info about character to file
        public void AddCharacterToFile(RPGCharacter newCharacter, string fileName)
        {
            try
            {
                using (StreamWriter writer = new StreamWriter(fileName, true))
                {
                    writer.WriteLine(newCharacter.ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }


        // adds the character to the database
        public void AddCharacterToDB(RPGCharacter newCharacter)
        {
            // insert query
            string sql = "INSERT INTO Character(Type, Name, HP, Energy, ArmorRating) "
                + "OUTPUT INSERTED.ID "
                + "VALUES (@Type, @Name, @HP, @Energy, @ArmorRating)";
            int charID = 0;
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.Parameters.AddWithValue("@Type", charTypes[cbCharacterSelection.SelectedIndex].ToString());
                        command.Parameters.AddWithValue("@Name", newCharacter.Name);
                        command.Parameters.AddWithValue("@HP", newCharacter.HP);
                        command.Parameters.AddWithValue("@Energy", newCharacter.Energy);
                        command.Parameters.AddWithValue("@ArmorRating", newCharacter.ArmorRating);
                        // get back id after insertion
                        charID = (int) command.ExecuteScalar();
                    }

                    
                }
            } catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            // add id to new character
            newCharacter.ID = charID;
        }

        // runs the sql query in given string
        public void RunSqlCmdNonQuery(string sql)
        {
            try
            {
                using (SqlConnection connection = new SqlConnection(builder.ConnectionString))
                {
                    connection.Open();

                    using (SqlCommand command = new SqlCommand(sql, connection))
                    {
                        command.ExecuteNonQuery();
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }


        // method that runs when a character is selected in the listbox
        // displays info about the character in the form
        private void lbCharacterDisplay_SelectedIndexChanged(object sender, EventArgs e)
        {
            // display to form
            RPGCharacter selectedChar = null;
            try
            {
                selectedChar = (RPGCharacter) lbCharacterDisplay.SelectedItem;
            } catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            if (selectedChar == null)
            {
                return;
            }
            // display info about the character
            tbEnterNewName.Text = selectedChar.Name;
            lblDisplayType.Text = "Type: " + selectedChar.CharacterType; 
            lblDisplayHP.Text = "HP: " + selectedChar.HP;
            lblDisplayEnergy.Text = "Energy: " + selectedChar.Energy;
            lblDisplayArmorRating.Text = "Armor Rating: " + selectedChar.ArmorRating;
        }


        // method for updating a character, called when the update button is clicked
        private void btnUpdate_Click(object sender, EventArgs e)
        {
            RPGCharacter selectedChar = null;
            try
            {
                selectedChar = (RPGCharacter) lbCharacterDisplay.SelectedItem;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            if (selectedChar != null && selectedChar.Name != tbEnterNewName.Text)
            {
                // update object with new name
                selectedChar.Name = tbEnterNewName.Text;
                // update listbox
                int lbIndex = lbCharacterDisplay.SelectedIndex;
                lbCharacterDisplay.BeginUpdate();
                lbCharacterDisplay.Items.RemoveAt(lbIndex);
                lbCharacterDisplay.Items.Insert(lbIndex, selectedChar);
                lbCharacterDisplay.EndUpdate();

                // update in database
                string sqlString = $"UPDATE Character SET Name = {tbEnterNewName.Text} WHERE ID = {selectedChar.ID}";
                RunSqlCmdNonQuery(sqlString);
                MessageBox.Show($"Updated name to: {tbEnterNewName.Text}");
            }
            // reset display
            resetCharacterInfoDisplay();
        }

        // resets the displayed info about a character
        public void resetCharacterInfoDisplay()
        {
            // reset
            tbEnterNewName.Text = "";
            lblDisplayType.Text = "Type:"; 
            lblDisplayHP.Text = "HP: ";
            lblDisplayEnergy.Text = "Energy: ";
            lblDisplayArmorRating.Text = "Armor Rating: ";
        }


        // delete a character from the database and listbox, called when the
        // delete button is clicked
        private void btnDelete_Click(object sender, EventArgs e)
        {
            RPGCharacter selectedChar = null;
            try
            {
                // getting the selected character
                selectedChar = (RPGCharacter) lbCharacterDisplay.SelectedItem;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                return;
            }
            if (selectedChar == null)
            {
                Console.WriteLine("Null character!");
                return;
            }
            // remove from listbox
            lbCharacterDisplay.BeginUpdate();
            lbCharacterDisplay.Items.RemoveAt(lbCharacterDisplay.SelectedIndex);
            lbCharacterDisplay.EndUpdate();
               
            // remove from database
            string sqlString = $"DELETE FROM Character WHERE ID = {selectedChar.ID}";
            RunSqlCmdNonQuery(sqlString);
            // reset display
            resetCharacterInfoDisplay();
            MessageBox.Show($"{selectedChar.Name} is removed");
        }
    }
}
