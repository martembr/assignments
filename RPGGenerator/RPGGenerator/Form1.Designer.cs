﻿namespace RPGGenerator
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cbCharacterSelection = new System.Windows.Forms.ComboBox();
            this.lblCharType = new System.Windows.Forms.Label();
            this.lblName = new System.Windows.Forms.Label();
            this.tbEnterName = new System.Windows.Forms.TextBox();
            this.btnCreate = new System.Windows.Forms.Button();
            this.lblCharactersDisplay = new System.Windows.Forms.Label();
            this.lbCharacterDisplay = new System.Windows.Forms.ListBox();
            this.lblSelectedChar = new System.Windows.Forms.Label();
            this.lblDisplayName = new System.Windows.Forms.Label();
            this.lblDisplayType = new System.Windows.Forms.Label();
            this.lblDisplayHP = new System.Windows.Forms.Label();
            this.lblDisplayEnergy = new System.Windows.Forms.Label();
            this.lblDisplayArmorRating = new System.Windows.Forms.Label();
            this.tbEnterNewName = new System.Windows.Forms.TextBox();
            this.btnUpdate = new System.Windows.Forms.Button();
            this.btnDelete = new System.Windows.Forms.Button();
            this.lblNewCharacter = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // cbCharacterSelection
            // 
            this.cbCharacterSelection.FormattingEnabled = true;
            this.cbCharacterSelection.Location = new System.Drawing.Point(44, 125);
            this.cbCharacterSelection.Name = "cbCharacterSelection";
            this.cbCharacterSelection.Size = new System.Drawing.Size(249, 28);
            this.cbCharacterSelection.TabIndex = 0;
            // 
            // lblCharType
            // 
            this.lblCharType.AutoSize = true;
            this.lblCharType.Location = new System.Drawing.Point(40, 71);
            this.lblCharType.Name = "lblCharType";
            this.lblCharType.Size = new System.Drawing.Size(229, 20);
            this.lblCharType.TabIndex = 1;
            this.lblCharType.Text = "Please select a character type: ";
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(40, 182);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(163, 20);
            this.lblName.TabIndex = 2;
            this.lblName.Text = "Please enter a name: ";
            // 
            // tbEnterName
            // 
            this.tbEnterName.Location = new System.Drawing.Point(44, 228);
            this.tbEnterName.Name = "tbEnterName";
            this.tbEnterName.Size = new System.Drawing.Size(249, 26);
            this.tbEnterName.TabIndex = 3;
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(44, 305);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(249, 49);
            this.btnCreate.TabIndex = 4;
            this.btnCreate.Text = "CREATE";
            this.btnCreate.UseVisualStyleBackColor = true;
            this.btnCreate.Click += new System.EventHandler(this.btnCreate_Click);
            // 
            // lblCharactersDisplay
            // 
            this.lblCharactersDisplay.AutoSize = true;
            this.lblCharactersDisplay.Location = new System.Drawing.Point(341, 25);
            this.lblCharactersDisplay.Name = "lblCharactersDisplay";
            this.lblCharactersDisplay.Size = new System.Drawing.Size(174, 20);
            this.lblCharactersDisplay.TabIndex = 5;
            this.lblCharactersDisplay.Text = "Registered characters: ";
            // 
            // lbCharacterDisplay
            // 
            this.lbCharacterDisplay.FormattingEnabled = true;
            this.lbCharacterDisplay.ItemHeight = 20;
            this.lbCharacterDisplay.Location = new System.Drawing.Point(345, 70);
            this.lbCharacterDisplay.Name = "lbCharacterDisplay";
            this.lbCharacterDisplay.Size = new System.Drawing.Size(187, 284);
            this.lbCharacterDisplay.TabIndex = 6;
            this.lbCharacterDisplay.SelectedIndexChanged += new System.EventHandler(this.lbCharacterDisplay_SelectedIndexChanged);
            // 
            // lblSelectedChar
            // 
            this.lblSelectedChar.AutoSize = true;
            this.lblSelectedChar.Location = new System.Drawing.Point(567, 25);
            this.lblSelectedChar.Name = "lblSelectedChar";
            this.lblSelectedChar.Size = new System.Drawing.Size(143, 20);
            this.lblSelectedChar.TabIndex = 7;
            this.lblSelectedChar.Text = "Selected character";
            // 
            // lblDisplayName
            // 
            this.lblDisplayName.AutoSize = true;
            this.lblDisplayName.Location = new System.Drawing.Point(567, 73);
            this.lblDisplayName.Name = "lblDisplayName";
            this.lblDisplayName.Size = new System.Drawing.Size(59, 20);
            this.lblDisplayName.TabIndex = 8;
            this.lblDisplayName.Text = "Name: ";
            // 
            // lblDisplayType
            // 
            this.lblDisplayType.AutoSize = true;
            this.lblDisplayType.Location = new System.Drawing.Point(567, 115);
            this.lblDisplayType.Name = "lblDisplayType";
            this.lblDisplayType.Size = new System.Drawing.Size(47, 20);
            this.lblDisplayType.TabIndex = 9;
            this.lblDisplayType.Text = "Type:";
            // 
            // lblDisplayHP
            // 
            this.lblDisplayHP.AutoSize = true;
            this.lblDisplayHP.Location = new System.Drawing.Point(567, 154);
            this.lblDisplayHP.Name = "lblDisplayHP";
            this.lblDisplayHP.Size = new System.Drawing.Size(35, 20);
            this.lblDisplayHP.TabIndex = 10;
            this.lblDisplayHP.Text = "HP:";
            // 
            // lblDisplayEnergy
            // 
            this.lblDisplayEnergy.AutoSize = true;
            this.lblDisplayEnergy.Location = new System.Drawing.Point(567, 192);
            this.lblDisplayEnergy.Name = "lblDisplayEnergy";
            this.lblDisplayEnergy.Size = new System.Drawing.Size(67, 20);
            this.lblDisplayEnergy.TabIndex = 11;
            this.lblDisplayEnergy.Text = "Energy: ";
            // 
            // lblDisplayArmorRating
            // 
            this.lblDisplayArmorRating.AutoSize = true;
            this.lblDisplayArmorRating.Location = new System.Drawing.Point(567, 234);
            this.lblDisplayArmorRating.Name = "lblDisplayArmorRating";
            this.lblDisplayArmorRating.Size = new System.Drawing.Size(107, 20);
            this.lblDisplayArmorRating.TabIndex = 12;
            this.lblDisplayArmorRating.Text = "Armor Rating:";
            // 
            // tbEnterNewName
            // 
            this.tbEnterNewName.Location = new System.Drawing.Point(633, 71);
            this.tbEnterNewName.Name = "tbEnterNewName";
            this.tbEnterNewName.Size = new System.Drawing.Size(141, 26);
            this.tbEnterNewName.TabIndex = 13;
            // 
            // btnUpdate
            // 
            this.btnUpdate.Location = new System.Drawing.Point(571, 293);
            this.btnUpdate.Name = "btnUpdate";
            this.btnUpdate.Size = new System.Drawing.Size(103, 61);
            this.btnUpdate.TabIndex = 14;
            this.btnUpdate.Text = "Update character";
            this.btnUpdate.UseVisualStyleBackColor = true;
            this.btnUpdate.Click += new System.EventHandler(this.btnUpdate_Click);
            // 
            // btnDelete
            // 
            this.btnDelete.Location = new System.Drawing.Point(685, 293);
            this.btnDelete.Name = "btnDelete";
            this.btnDelete.Size = new System.Drawing.Size(103, 61);
            this.btnDelete.TabIndex = 15;
            this.btnDelete.Text = "Delete character";
            this.btnDelete.UseVisualStyleBackColor = true;
            this.btnDelete.Click += new System.EventHandler(this.btnDelete_Click);
            // 
            // lblNewCharacter
            // 
            this.lblNewCharacter.AutoSize = true;
            this.lblNewCharacter.Location = new System.Drawing.Point(44, 25);
            this.lblNewCharacter.Name = "lblNewCharacter";
            this.lblNewCharacter.Size = new System.Drawing.Size(174, 20);
            this.lblNewCharacter.TabIndex = 16;
            this.lblNewCharacter.Text = "Create a new character";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(829, 410);
            this.Controls.Add(this.lblNewCharacter);
            this.Controls.Add(this.btnDelete);
            this.Controls.Add(this.btnUpdate);
            this.Controls.Add(this.tbEnterNewName);
            this.Controls.Add(this.lblDisplayArmorRating);
            this.Controls.Add(this.lblDisplayEnergy);
            this.Controls.Add(this.lblDisplayHP);
            this.Controls.Add(this.lblDisplayType);
            this.Controls.Add(this.lblDisplayName);
            this.Controls.Add(this.lblSelectedChar);
            this.Controls.Add(this.lbCharacterDisplay);
            this.Controls.Add(this.lblCharactersDisplay);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.tbEnterName);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.lblCharType);
            this.Controls.Add(this.cbCharacterSelection);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cbCharacterSelection;
        private System.Windows.Forms.Label lblCharType;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.TextBox tbEnterName;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.Label lblCharactersDisplay;
        private System.Windows.Forms.ListBox lbCharacterDisplay;
        private System.Windows.Forms.Label lblSelectedChar;
        private System.Windows.Forms.Label lblDisplayName;
        private System.Windows.Forms.Label lblDisplayType;
        private System.Windows.Forms.Label lblDisplayHP;
        private System.Windows.Forms.Label lblDisplayEnergy;
        private System.Windows.Forms.Label lblDisplayArmorRating;
        private System.Windows.Forms.TextBox tbEnterNewName;
        private System.Windows.Forms.Button btnUpdate;
        private System.Windows.Forms.Button btnDelete;
        private System.Windows.Forms.Label lblNewCharacter;
    }
}

