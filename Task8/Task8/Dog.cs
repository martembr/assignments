﻿using System;

namespace Task8
{

    public abstract class Dog : Animal, ITrainable, IRunning
    {

        public string FavoriteToy { get; set; }

        public Dog() : base() { }
        public Dog(string name, int age, string toy) : base(name, age)
        {
            FavoriteToy = toy;
        }

        public override void MakeSound()
        {
            Console.WriteLine("woof!");
        }

        public abstract void Play();


        public override string ToString()
        {
            return base.ToString() + $", favorite toy: {FavoriteToy}";
        }

        public void Train(string task)
        {
            Console.WriteLine($"Practicing {task}");
        }

        public void Move()
        {
            Console.WriteLine("Happily runs forwards");
        }
    }
}
