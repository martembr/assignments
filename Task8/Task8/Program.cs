﻿using System;
using System.Collections.Generic;

namespace Task8
{
    class Program
    {
        static void Main(string[] args)
        {
            Dog dog1 = new Poodle("Ted", 3, "stuffed animal");
            Dog dog2 = new GoldenRetriever("Lola", 6, "tennis ball");
            Cat cat1 = new Cat("Kurt", 4);
            Cat cat2 = new Cat("Mao", 2);


            Console.WriteLine("\nTraining:");
            Train(dog1, dog2);
            Console.WriteLine("\nClimbing");
            Climb(cat1, cat2);
            Console.WriteLine("\nRunning:");
            Running(dog1, cat1, dog2, cat2);

        }


        public static void Train(params ITrainable[] animals)
        {
            foreach (ITrainable animal in animals)
            {
                animal.Train("sit");
            }
        }

        public static void Running(params IRunning[] animals)
        {
            foreach (IRunning animal in animals)
            {
                animal.Move();
            }
        }


        public static void Climb(params IClimber[] animals)
        {
            foreach (IClimber animal in animals)
            {
                animal.Climb();
            }
        }

    }
}
