﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Task3
{
    class Program
    {
        static void Main(string[] args)
        {
            // initialize dictionary 
            Dictionary<int, string> phonebook = new Dictionary<int, string>()
            {
                {41526374, "Kari Normann"},
                {98765432, "Ola Normann"},
                {49382716, "Ola Normann"},
                {90807060, "Per Persson"},
                {40506070, "Siri Hansen"}
            };
            Console.WriteLine("Welcome to Yellow Pages!");
            string menuText = "\nPlease choose an option: \n" +
                "n - search by name \n" +
                "p - search by phone number\n" + 
                "e - exit \n ";
            bool endSearch = false;
            String choice = ""; 

            // menu loop
            while (!endSearch)
            {
                Console.WriteLine(menuText);
                choice = Console.ReadLine();
                switch (choice)
                {
                    case "n":
                        Console.WriteLine("Please enter a name");
                        string name = Console.ReadLine();
                        FindContactByName(phonebook, name);
                        break;
                    case "p":
                        Console.WriteLine("Please enter a phone number:\n");
                        try
                        {
                            int numb = Convert.ToInt32(Console.ReadLine());
                            FindContactByNumber(phonebook, numb);
                        }
                        catch (FormatException ex)
                        {
                            Console.WriteLine("What you have entered was not a number\n");
                        }
                        break;
                    case "e":
                        endSearch = true;
                        break;
                    default:
                        Console.WriteLine("Unknown choice - please try again!\n");
                        break;
                }
            }
        }

        static void FindContactByNumber(Dictionary<int, string> contacts, int numb)
        {
            // Test if number is in the contact dictionary
            if (contacts.Keys.Contains(numb))
            {
                Console.WriteLine("The number {0} belongs to {1}\n", numb, contacts[numb]);
            } else
            {
                Console.WriteLine("No match was found for the number {0}\n", numb);
            }
        }


        static void FindContactByName(Dictionary<int, string> contacts, string nameToFind)
        {
            List<int> exactMatches = new List<int>();
            List<int> partialMatches = new List<int>();
            foreach (var contact in contacts)
            {
                string name = contact.Value.ToLower();
                // test for exact match
                if (name == nameToFind.ToLower())
                {
                    exactMatches.Add(contact.Key);
                }
                // test for partial match
                else
                {
                    foreach (char c in nameToFind.ToLower())
                    {
                        if (name.Contains(c))
                        {
                            partialMatches.Add(contact.Key);
                        }
                    }
                }
            }

            // write out exact matches if any was found
            if (exactMatches.Count > 0)
            {
                Console.WriteLine("\nExact matches found: ");
                foreach (int numb in exactMatches)
                {
                    Console.WriteLine("{0}: {1}", contacts[numb], numb);
                }
            }

            // write out partial matches if any was found
            if (partialMatches.Count > 0)
            {
                Console.WriteLine("\nPartial matches found: ");
                foreach (int numb in partialMatches)
                {
                    Console.WriteLine("{0}: {1}", contacts[numb], numb);
                }
            }

            // if no matches found
            if (partialMatches.Count == 0 && exactMatches.Count == 0)
            {
                Console.WriteLine("No matches found for {0}! \n", nameToFind);
            }
        }


            // old version - checks for if name is exactly equal or if first or last name is equal
            static void FindContactByName2(Dictionary<int, string> contacts, string nameToFind)
        {
            List<int> exactMatches = new List<int>();
            List<int> partialMatches = new List<int>();
            string[] names = nameToFind.Split(" ");
            foreach (var contact in contacts)
            {
                // test for exact match
                if (contact.Value.ToLower() == nameToFind.ToLower())
                {
                    exactMatches.Add(contact.Key);
                } 
                // test for match with first name
                else if (contact.Value.ToLower().Contains(names[0].ToLower())) 
                {
                    partialMatches.Add(contact.Key);
                } 
                // test for match with last name
                else if (names.Length > 1 && contact.Value.ToLower().Contains(names[1].ToLower()))
                {
                    partialMatches.Add(contact.Key);
                }
            }

            // write out exact matches if any was found
            if (exactMatches.Count > 0)
            {
                Console.WriteLine("\nExact matches found: ");
                foreach (int numb in exactMatches)
                {
                    Console.WriteLine("{0}: {1}", contacts[numb], numb);
                }
            }

            // write out partial matches if any was found
            if (partialMatches.Count > 0)
            {
                Console.WriteLine("\nPartial matches found: ");
                foreach (int numb in partialMatches)
                {
                    Console.WriteLine("{0}: {1}", contacts[numb], numb);
                }
            }

            // if no matches found
            if (partialMatches.Count == 0 && exactMatches.Count == 0)
            {
                Console.WriteLine("No matches found for {0}! \n", nameToFind);
            }
        }
    }
}
