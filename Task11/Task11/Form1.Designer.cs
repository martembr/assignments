﻿namespace Task11
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblChooseChar = new System.Windows.Forms.Label();
            this.cbCharacterType = new System.Windows.Forms.ComboBox();
            this.lblChooseName = new System.Windows.Forms.Label();
            this.tbName = new System.Windows.Forms.TextBox();
            this.btnCreate = new System.Windows.Forms.Button();
            this.lblCharacterHeader = new System.Windows.Forms.Label();
            this.lbCharacters = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // lblChooseChar
            // 
            this.lblChooseChar.AutoSize = true;
            this.lblChooseChar.Location = new System.Drawing.Point(39, 40);
            this.lblChooseChar.Name = "lblChooseChar";
            this.lblChooseChar.Size = new System.Drawing.Size(288, 20);
            this.lblChooseChar.TabIndex = 0;
            this.lblChooseChar.Text = "Please choose which type of character: ";
            this.lblChooseChar.Click += new System.EventHandler(this.label1_Click);
            // 
            // cbCharacterType
            // 
            this.cbCharacterType.FormattingEnabled = true;
            this.cbCharacterType.Location = new System.Drawing.Point(43, 80);
            this.cbCharacterType.Name = "cbCharacterType";
            this.cbCharacterType.Size = new System.Drawing.Size(284, 28);
            this.cbCharacterType.TabIndex = 1;
            // 
            // lblChooseName
            // 
            this.lblChooseName.AutoSize = true;
            this.lblChooseName.Location = new System.Drawing.Point(39, 144);
            this.lblChooseName.Name = "lblChooseName";
            this.lblChooseName.Size = new System.Drawing.Size(163, 20);
            this.lblChooseName.TabIndex = 2;
            this.lblChooseName.Text = "Please enter a name: ";
            // 
            // tbName
            // 
            this.tbName.Location = new System.Drawing.Point(43, 189);
            this.tbName.Name = "tbName";
            this.tbName.Size = new System.Drawing.Size(284, 26);
            this.tbName.TabIndex = 3;
            // 
            // btnCreate
            // 
            this.btnCreate.Location = new System.Drawing.Point(43, 258);
            this.btnCreate.Name = "btnCreate";
            this.btnCreate.Size = new System.Drawing.Size(284, 39);
            this.btnCreate.TabIndex = 4;
            this.btnCreate.Text = "Create";
            this.btnCreate.UseVisualStyleBackColor = true;
            // 
            // lblCharacterHeader
            // 
            this.lblCharacterHeader.AutoSize = true;
            this.lblCharacterHeader.Location = new System.Drawing.Point(498, 39);
            this.lblCharacterHeader.Name = "lblCharacterHeader";
            this.lblCharacterHeader.Size = new System.Drawing.Size(95, 20);
            this.lblCharacterHeader.TabIndex = 5;
            this.lblCharacterHeader.Text = "Characters: ";
            // 
            // lbCharacters
            // 
            this.lbCharacters.FormattingEnabled = true;
            this.lbCharacters.ItemHeight = 20;
            this.lbCharacters.Location = new System.Drawing.Point(502, 80);
            this.lbCharacters.Name = "lbCharacters";
            this.lbCharacters.Size = new System.Drawing.Size(212, 204);
            this.lbCharacters.TabIndex = 6;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(818, 345);
            this.Controls.Add(this.lbCharacters);
            this.Controls.Add(this.lblCharacterHeader);
            this.Controls.Add(this.btnCreate);
            this.Controls.Add(this.tbName);
            this.Controls.Add(this.lblChooseName);
            this.Controls.Add(this.cbCharacterType);
            this.Controls.Add(this.lblChooseChar);
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblChooseChar;
        private System.Windows.Forms.ComboBox cbCharacterType;
        private System.Windows.Forms.Label lblChooseName;
        private System.Windows.Forms.TextBox tbName;
        private System.Windows.Forms.Button btnCreate;
        private System.Windows.Forms.Label lblCharacterHeader;
        private System.Windows.Forms.ListBox lbCharacters;
    }
}

