﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task9
{
    public class ChaosArrayException : Exception
    {
        public ChaosArrayException(String msg) : base(msg) { }
    }


    public class UnavaibleSpaceException : ChaosArrayException
    {
        public UnavaibleSpaceException(String msg) : base(msg) { }
        public UnavaibleSpaceException() : base("Attempted to insert into unavailable space") { }
    }

    public class ItemNotFoundExcpetion : ChaosArrayException
    {
        public ItemNotFoundExcpetion(String msg) : base(msg) { }
        public ItemNotFoundExcpetion() : base("No item found") { }
    }
}
