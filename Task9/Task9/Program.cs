﻿using System;

namespace Task9
{
    class Program
    {
        static void Main(string[] args)
        {
            ChaosArray<int> numberChaosArr = new ChaosArray<int>();
            ChaosArray<string> stringChaosArr = new ChaosArray<string>();
            Console.WriteLine("Adding numbers to chaos array: ");
            for (int i = 0; i < 10; i++)
            {
                try
                {
                    numberChaosArr.Add(i + 10);
                    stringChaosArr.Add("hei");
                } 
                catch (ChaosArrayException ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            Console.WriteLine("Retrieving items from chaos array");
            for (int i = 0; i < 20; i++)
            {
                try
                {
                    Console.Write("Retrieved item: ");
                    Console.WriteLine(numberChaosArr.Retrieve());
                    Console.Write("Retrieved item: ");
                    Console.WriteLine(stringChaosArr.Retrieve());
                }
                catch (ChaosArrayException ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

        }
    }
}
