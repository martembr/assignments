﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Task9
{
    class ChaosArray<T>
    {
        private T[] chaosArr = new T[10];
        private Random rand = new Random();


        public void Add(T newItem)
        {
            // adding item randomly
            int index = rand.Next(chaosArr.Length);
            if (chaosArr[index] == null || chaosArr[index].Equals(default(T)))
            {
                chaosArr[index] = newItem;  
            }
            else
            {
                throw new UnavaibleSpaceException();
            }
        }

        public T Retrieve()
        {
            // retrieve item randomly
            int index = rand.Next(chaosArr.Length);
            if (chaosArr[index]==null || chaosArr[index].Equals(default(T)))
            {
                throw new ItemNotFoundExcpetion();
            }
            return chaosArr[index];
        }


    }


}
