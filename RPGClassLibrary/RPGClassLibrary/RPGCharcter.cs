﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGClassLibrary
{
    public abstract class RPGCharacter
    {
        // properties 
        public int ID { get; set; }
        public int HP { get; set; } 
        public int Energy { get; set; }
        public string Name { get; set; }
        public int ArmorRating { get; set; }

        public string CharacterType { get; set; }

        public RPGCharacter()
        {
            HP = 100;
            Energy = 100;
        }


        // default behavoir for attack
        public virtual void Attack()
        {
            Console.WriteLine("Attacking");
        }

        public abstract void Move();

        public override string ToString()
        {
            return $"{Name} ({CharacterType})";
        }
    }
}
