﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGClassLibrary
{
    public class Warrior : RPGCharacter
    {

        public Warrior() : base()
        {
            ArmorRating = 50;
            CharacterType = "Warrior";
        }

        public override void Move()
        {
            Console.WriteLine("Runs towards danger");
        }
    }
}
