﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGClassLibrary
{
    public class Thief : RPGCharacter
    {
        public Thief() : base()
        {
            ArmorRating = 5;
            CharacterType = "Thief";
        }

        public override void Move()
        {
            Console.WriteLine("Sneaking around");
        }

        public override void Attack()
        {
            Console.WriteLine("Stabs the enemy in the back");
        }
    }
}
