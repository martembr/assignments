﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace RPGClassLibrary
{
    public class Wizard : RPGCharacter
    {
        public Wizard() : base()
        {
            ArmorRating = 20;
            CharacterType = "Wizard";
        }
        public override void Move()
        {
            Console.WriteLine("Disappear from one spot and appears at another");
        }
    }
}
