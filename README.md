# Assignments overview

1. Hello You - FirstApp
2. Print Square - Task 2
3. Yellow Pages - Task 3
4. Module Upgrade - Task 4
5. Zoology - Zoology
6. Yellow Pages 2.0 - YellowPages
7. Zoology 2.0 - Zoology
8. Zoology 3.0 - Task 8
9. Chaos Array - Task 9 
10. Zoology 4.0 - Task 10
11. RPG Character Generator - RPGClassLibrary and RPGGenerator
12. Postgrad EF - Task 12
13. Movie Character API - Task 13
14. Docker - 