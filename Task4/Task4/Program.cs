﻿using System;

namespace Task4
{
    class Program
    {
        static void Main(string[] args)
        {
            string menuText = "Choose between draw squares or yellow pages (e to exit)\n" +
                "d - draw squares \ny - yellow pages\n";
            string choice = "";
            while (choice != "e")
            {
                Console.WriteLine(menuText);
                choice = Console.ReadLine();
                if (choice == "d")
                {
                    // draw sq
                    DrawSquares.Menu();
                } 
                else if (choice == "y")
                { 
                    YellowPages.Menu();
                } else if (choice != "e")
                {
                    Console.WriteLine("Unknown choice");
                }
            }
        }
    }
}
