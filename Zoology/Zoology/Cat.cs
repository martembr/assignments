﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoology
{
    public class Cat : Animal 
    {

        public Cat() : base() { }
        public Cat(string name, int age) : base(name, age) { }

        public override void MakeSound()
        {
            Console.WriteLine("meow!");
        }
    }
}
