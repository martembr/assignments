﻿using System;
using System.Collections.Generic;
using System.Text;


namespace Zoology
{
    public class GoldenRetriever : Dog
    {
        public GoldenRetriever() : base() { }
        public GoldenRetriever(string name, int age, string toy) : base(name, age, toy) { }


        public override void Play()
        {
            Console.WriteLine("Playing fetch with a tennis ball in the water");
        }
    }
}