﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoology
{
    public class Poodle : Dog
    {
        public Poodle() : base() { }
        public Poodle(string name, int age, string toy) : base(name, age, toy) { }


        public override void Play()
        {
            Console.WriteLine("Jumping around with other poodles");
        }
    }
}
