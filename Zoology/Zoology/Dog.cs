﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zoology
{
    public abstract class Dog : Animal
    {

        public string FavoriteToy { get; set; }

        public Dog() : base() { }
        public Dog(string name, int age, string toy) : base(name, age) 
        {
            FavoriteToy = toy;
        }

        public override void MakeSound()
        {
            Console.WriteLine("woof!");
        }

        public abstract void Play();


        public override string ToString()
        {
            return base.ToString() + $", favorite toy: {FavoriteToy}";
        }
    }
}
