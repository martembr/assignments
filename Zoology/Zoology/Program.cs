﻿using System;
using System.Collections.Generic;

namespace Zoology
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Animal> animals = new List<Animal>();
            Dog dog1 = new Poodle("Ted", 3, "stuffed animal");
            Dog dog2 = new GoldenRetriever("Lola", 6, "tennis ball");
            animals.Add(new Cat("Kurt", 4));
            animals.Add(dog1);
            animals.Add(dog2);
            animals.Add(new Cat("Mao", 2));

            foreach(Animal animal in animals)
            {
                Console.Write("The animal: ");
                Console.WriteLine(animal);
                Console.Write("It says: ");
                animal.MakeSound();
            }

            Console.WriteLine("The dogs play: ");
            dog1.Play();
            dog2.Play();

        }
    }
}
