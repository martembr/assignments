﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.IO;

namespace ExcerciseFiles
{


    class Program
    {

        static List<string> toDoList = new List<string>();
        static List<Movie> movieCatalog = new List<Movie>();

        static void Main(string[] args)
        {
            // add all our favorite movies
            movieCatalog.Add(new Movie() { Title = "Sharknado I", Genre = "Thriller Comedy", Rating = 3, ReleaseYear = 2000 });
            movieCatalog.Add(new Movie() { Title = "Sharknado I", Genre = "Thriller Comedy", Rating = 3, ReleaseYear = 2000 });
            movieCatalog.Add(new Movie() { Title = "Sharknado II", Genre = "Thriller Comedy", Rating = 2, ReleaseYear = 2003 });
            movieCatalog.Add(new Movie() { Title = "Work It", Genre = "Feel good", Rating = 5, ReleaseYear = 2020 });
            movieCatalog.Add(new Movie() { Title = "Christmas Knight", Genre = "Comedy", Rating = 2, ReleaseYear = 2019 });

            // add tasks to todo list
            toDoList.Add("Wake up");
            toDoList.Add("Make coffee");
            toDoList.Add("Drink coffee");
            toDoList.Add("Get ready");
            toDoList.Add("Make second cup of coffee");
            toDoList.Add("Drink more coffee");
            
            // Store todo list in a txt file 
            try
            {
                using (StreamWriter streamWriter = new StreamWriter(@"toDo.txt"))
                {
                    streamWriter.WriteLine("My todo list: ");
                    foreach (string task in toDoList)
                    {
                        streamWriter.WriteLine(task);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            // read from txt file to console
            try
            {
                using (StreamReader streamReader = new StreamReader(@"toDo.txt"))
                {
                    string line = streamReader.ReadToEnd();
                    Console.WriteLine(line);
                }
            }
            catch (FileNotFoundException ex)
            {
                Console.WriteLine(ex.Message);
            }

            // store movie catalog in CSV file with proper headings 
            try
            {
                using (StreamWriter writer = new StreamWriter("Movies.csv"))
                {
                    // funker ikke!
                    using (CsvWriter csv = new CsvWriter(writer, System.Globalization.CultureInfo.CurrentCulture))
                    {
                        csv.WriteRecords<Movie>(movieCatalog);
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }


            // read back from csv
            try
            {
                using (StreamReader reader = new StreamReader("Movies.csv"))
                {
                    using (CsvReader csv = new CsvReader(reader, System.Globalization.CultureInfo.CurrentCulture))
                    {
                        IEnumerable<Movie> movies = csv.GetRecords<Movie>();
                        foreach (Movie mov in movies)
                        {
                            Console.WriteLine(mov);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }


        }
    }
}
