﻿using System;
using System.Collections.Generic;
using System.Text;

namespace ExcerciseFiles
{
    class Movie
    {
        public string Title { get; set; }
        public int ReleaseYear { get; set; }
        public int Rating { get; set; }
        public string Genre { get; set; }


        public override string ToString()
        {
            return $"{Title} ({ReleaseYear}), Genre: {Genre}, Rating: {Rating}";
        }
    }


}
