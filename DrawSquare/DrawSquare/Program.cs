﻿using System;

namespace DrawSquare
{
    class Program
    {
        static void Main(string[] args)
        {
            // start the menu
            Menu();
        }

        // responsible for printing the menu in the console and calling appropriate methods
        static void Menu()
        {
            string menuText = "\nWelcome to the square/rectangle printer\n" +
                "Please choose if you want to draw a square or a rectangle: \n" +
                "s - draw square \nr - draw rectangle \ne - exit\n";
            string choice = "";
            while (choice != "e")
            {
                Console.WriteLine(menuText);
                choice = Console.ReadLine();
                switch (choice)
                {
                    case "s":
                        Console.WriteLine("Drawing a square!");
                        int sizeOfSquare = TakeInNumber("size");
                        DrawInConsole(sizeOfSquare, sizeOfSquare);
                        break;
                    case "r":
                        Console.WriteLine("Drawing a rectangle");
                        int heigth = TakeInNumber("height");
                        int width = TakeInNumber("width");
                        DrawInConsole(heigth, width);
                        break;
                    case "e":
                        Console.WriteLine("Goodbye!");
                        break;
                    default:
                        Console.WriteLine("Unknown option, please try again!");
                        break;
                }
            }
        }


        // takes in a number from user and makes sure its a valid value
        static int TakeInNumber(string type)
        {
            Console.WriteLine("Please enter the {0}", type);
            int numb = 0;
            while (numb <= 0)
            {
                try
                {
                    numb = Convert.ToInt32(Console.ReadLine());
                }
                catch (Exception)
                {
                    Console.WriteLine("Please enter a postive integer value ");
                }
            }
            return numb;
        }

        // draws the square/rectangle in the console
        static void DrawInConsole(int height, int width)
        {
            for (int i = 0; i < height; i++)
            {
                for (int j = 0; j < width; j++)
                {
                    if (i == 0 || i == height - 1 || j == 0 || j == width - 1)
                    {
                        Console.Write("#");
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                }
                Console.WriteLine(" ");
            }
        }
    }
}
