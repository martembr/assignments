﻿using System;

namespace Task2
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Welcome to a square printer!");
            Console.WriteLine("Please enter the size of the square");
            int sizeOfSquare = 0;
            while (sizeOfSquare == 0)
            {
                try
                {
                    sizeOfSquare = Convert.ToInt32(Console.ReadLine());
                } catch (Exception)
                {
                    Console.WriteLine("Please enter an integer value ");
                }
            }
            for (int i = 0; i < sizeOfSquare; i++)
            {
                for (int j = 0; j < sizeOfSquare; j++)
                {
                    if (i == 0 || i == sizeOfSquare-1 || j == 0 || j== sizeOfSquare-1)
                    {
                        Console.Write("#");
                    }
                    else
                    {
                        Console.Write(" ");
                    }
                }
                Console.WriteLine(" ");
            }
        }
    }
}
