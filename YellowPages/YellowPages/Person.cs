﻿using System;
using System.Collections.Generic;
using System.Text;

namespace YellowPages
{
    class Person
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int PhoneNumber { get; set; }


        public Person(string firstName, string lastName, int phonenumber)
        {
            this.FirstName = firstName;
            this.LastName = lastName;
            this.PhoneNumber = phonenumber; 
        }

        // returns true if phonenumber is equal 
        public bool HasPhoneNumber(int number)
        {
            return number == PhoneNumber;
        }

        // returns true if first name is equal
        public bool HasFirstName(string fname)
        {
            return fname.ToLower() == FirstName.ToLower();
        }


        // returns true if lastname is equal
        public bool HasLastName(string lname)
        {
            return lname.ToLower() == LastName.ToLower();
        }


        // returns true if partial match to first or last name
        public bool HasPartialMatch(string name)
        {
            string[] names = name.ToLower().Split(" ");
            foreach(string n in names)
            {
                if (FirstName.ToLower().Contains(n) || LastName.ToLower().Contains(n))
                {
                    return true;
                }
            }
            return false;
        }

        public override string ToString()
        {
            return $"{FirstName} {LastName}: {PhoneNumber}";
        }

    }
}
