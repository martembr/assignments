﻿using System;
using System.Collections.Generic;

namespace YellowPages
{
    class Program
    {
        static List<Person> people;

        static void Main(string[] args)
        {
            people = InitializePhoneBook();
            Menu();
        }

        static List<Person> InitializePhoneBook()
        {
            List<Person> people = new List<Person>();
            people.Add(new Person("Kari", "Normann", 41526374));
            people.Add(new Person("Ola", "Normann", 49382716));
            people.Add(new Person("Ola", "Normann", 98765432));
            people.Add(new Person("Per", "Persson", 90807060));
            people.Add(new Person("Siri", "Hansen", 40506070));
            return people;
        }


        public static void Menu()
        {
            Console.WriteLine("Welcome to Yellow Pages!");
            string menuText = "\nPlease choose an option: \n" +
                "v - view all entries \n" +
                "n - search by name \n" +
                "p - search by phone number\n" +
                "e - exit \n ";
            bool endSearch = false;
            String choice = "";

            // menu loop
            while (!endSearch)
            {
                Console.WriteLine(menuText);
                choice = Console.ReadLine();
                switch (choice)
                {
                    case "v":
                        Console.WriteLine("\nAll contacts:");
                        foreach (Person contact in people)
                        {
                            Console.WriteLine(contact);
                        }
                        break;
                    case "n":
                        Console.WriteLine("Please enter a name");
                        string name = Console.ReadLine();
                        FindContactByName(people, name);
                        break;
                    case "p":
                        Console.WriteLine("Please enter a phone number:\n");
                        try
                        {
                            int numb = Convert.ToInt32(Console.ReadLine());
                            FindContactByNumber(people, numb);
                        }
                        catch (FormatException ex)
                        {
                            Console.WriteLine("What you have entered was not a number\n");
                        }
                        break;
                    case "e":
                        endSearch = true;
                        break;
                    default:
                        Console.WriteLine("Unknown choice - please try again!\n");
                        break;
                }
            }
        }

        static void FindContactByName(List<Person> contacts, string name)
        {
            List<Person> exactMatches = new List<Person>();
            List<Person> partialMatches = new List<Person>();
            string[] names = name.Split(" ");
            foreach (Person pers in contacts)
            {
                if (names.Length == 2 && pers.HasFirstName(names[0]) && pers.HasLastName(names[1]))
                {
                    exactMatches.Add(pers);
                }
                else if (pers.HasPartialMatch(name))
                {
                    partialMatches.Add(pers);
                }
            }

            // write out exact matches if any was found
            if (exactMatches.Count > 0)
            {
                Console.WriteLine("\nExact matches found: ");
                foreach (Person pers in exactMatches)
                {
                    Console.WriteLine(pers);
                }
            }

            // write out partial matches if any was found
            if (partialMatches.Count > 0)
            {
                Console.WriteLine("\nPartial matches found: ");
                foreach (Person pers in partialMatches)
                {
                    Console.WriteLine(pers);
                }
            }

            // if no matches found
            if (partialMatches.Count == 0 && exactMatches.Count == 0)
            {
                Console.WriteLine("No matches found for {0}! \n", name);
            }
        }


        static void FindContactByNumber(List<Person> contacts, int number)
        {
            foreach (Person pers in contacts)
            {
                if (pers.HasPhoneNumber(number))
                {
                    Console.WriteLine("Found a match: ");
                    Console.WriteLine(pers);
                }
            }
        }

    }
}
