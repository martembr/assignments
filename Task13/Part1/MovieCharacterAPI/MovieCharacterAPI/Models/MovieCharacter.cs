﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieCharacterAPI.Models
{
    public class MovieCharacter
    {
        public int MovieId { get; set; }
        public int ActorId { get; set; } 

        public int CharacterId { get; set; }
        public string PictureURL { get; set; }
    }
}
