﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;


namespace MovieCharacterAPI.Models
{
    public class MovieCharacterDbContext : DbContext     
    {
        
        public DbSet<Actor> Actor { get; set; }
        public DbSet<Character> Character { get; set; }
        public DbSet<Franchise> Franchise { get; set; }
        public DbSet<Movie> Movie { get; set; }
        public DbSet<MovieCharacter> MovieCharacter { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source =PC7389\\SQLEXPRESS;Initial Catalog=MovieCharacterDB;Integrated Security=True;");
        }

        // set key for MovieCharacter as combination of id's
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            
            modelBuilder.Entity<MovieCharacter>().HasKey(mc => new { mc.MovieId, mc.CharacterId });
            // seed data
            modelBuilder.Entity<Franchise>().HasData(new Franchise {Id = 1, Name = "Star Wars", Description = "Science fiction movies" });
            modelBuilder.Entity<Franchise>().HasData(new Franchise {Id = 2, Name = "The Lord of the Rings", Description = "From the book by Tolkien" });
            
            
            modelBuilder.Entity<Movie>().HasData(new Movie {Id = 1, Title = "The Fellowship of the Ring", ReleaseYear = 2001 , Director = "Peter Jackson", FranchiseId = 2, Genre = "Fantasy"});



            
        }
    }
}
