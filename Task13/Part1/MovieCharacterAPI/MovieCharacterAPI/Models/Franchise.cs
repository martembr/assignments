﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieCharacterAPI.Models
{
    public class Franchise
    {
        // primary key
        public int Id { get; set; }

        public String Name { get; set; }
        public String Description { get; set; }


        // navigation
        public ICollection<Movie> Movies { get; set; }
    }
}
