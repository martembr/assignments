﻿using AutoMapper;
using MovieCharacterAPI.DTOs.Character;
using MovieCharacterAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Profiles
{
    public class CharacterProfile : Profile
    {
        public CharacterProfile()
        {
            CreateMap<Character, CharacterDto>().ReverseMap();
            CreateMap<Character, CharacterListDto>();

            // CreateMap<Character, CharacterActorsDto>();....
        }
    }
}
