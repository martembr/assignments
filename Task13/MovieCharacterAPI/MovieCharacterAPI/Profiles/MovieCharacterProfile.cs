﻿using AutoMapper;
using MovieCharacterAPI.DTOs.MovieCharacter;
using MovieCharacterAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Profiles
{
    public class MovieCharacterProfile : Profile
    {
        public MovieCharacterProfile()
        {
            CreateMap<MovieCharacter, MovieCharacterDto>().ReverseMap();
            CreateMap<MovieCharacter, MovieCharacterActorDto>().ForMember(mcadto => mcadto.Actor, opt => opt.MapFrom(mca => mca.Actor.FirstName + " " + mca.Actor.LastNames)); 
            CreateMap<MovieCharacter, MovieCharacterCharacterDto>().ForMember(mcadto => mcadto.Character, opt => opt.MapFrom(mcc => mcc.Character.FullName));
            CreateMap<MovieCharacter, MovieCharacterMovieDto>().ForMember(mcadto => mcadto.Movie, opt => opt.MapFrom(mcm => mcm.Movie.Title));
            CreateMap<MovieCharacter, MovieCharacterListDto>().ForMember(mcldto => mcldto.Movie, opt => opt.MapFrom(mcm => mcm.Movie.Title))
                .ForMember(mcldto => mcldto.Character, opt => opt.MapFrom(mcc => mcc.Character.FullName))
                .ForMember(mcldto => mcldto.Actor, opt => opt.MapFrom(mca => mca.Actor.FirstName + " " + mca.Actor.LastNames));
        }
    }
}
