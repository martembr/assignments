﻿using AutoMapper;
using MovieCharacterAPI.DTOs.Movie;
using MovieCharacterAPI.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.Profiles
{
    public class MovieProfile : Profile 
    {
        public MovieProfile()
        {
            CreateMap<Movie, MovieDto>().ReverseMap();   
            CreateMap<Movie, MovieListDto>();   
            CreateMap<Movie, MovieFranchiseDto>().ForMember(mfdto => mfdto.Franchise, opt => opt.MapFrom(m => m.Franchise.Name));   
        }
    }
}
