﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MovieCharacterAPI.Models
{
    public class Character
    {
        // primary key
        public int Id { get; set; }

        public string FullName { get; set; }
        public string Alias { get; set; } // optional
        public string Gender { get; set; }
        public string PictureURL { get; set; }


        // navigation
        public ICollection<MovieCharacter> Moviecharacters { get; set; }

    }
}
