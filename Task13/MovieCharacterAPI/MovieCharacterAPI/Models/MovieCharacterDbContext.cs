﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;


namespace MovieCharacterAPI.Models
{
    public class MovieCharacterDbContext : DbContext     
    {
        
        // override constructor
        public MovieCharacterDbContext(DbContextOptions builder) : base(builder) { }

        public DbSet<Actor> Actor { get; set; }
        public DbSet<Character> Character { get; set; }
        public DbSet<Franchise> Franchise { get; set; }
        public DbSet<Movie> Movie { get; set; }
        public DbSet<MovieCharacter> MovieCharacter { get; set; }


        /*
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer("Data Source =PC7389\\SQLEXPRESS;Initial Catalog=MovieCharacterDB;Integrated Security=True;");
        }
        */


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // set key for MovieCharacter as combination of id's
            modelBuilder.Entity<MovieCharacter>().HasKey(mc => new { mc.MovieId, mc.CharacterId });

            // seed with data

            // Franchises
            modelBuilder.Entity<Franchise>().HasData(new Franchise { Id = 1, Name = "Star Wars", Description = "In a galaxy far far away ..." });
            modelBuilder.Entity<Franchise>().HasData(new Franchise { Id = 2, Name = "The Lord of the Rings", Description = "Film adaption of the book of the same name by J.R.R. Tolkien" });

            // LoTR movies
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 1, Title = "The Fellowship of the Ring", ReleaseYear = new DateTime(2001, 1, 1), Director = "Peter Jackson", FranchiseId = 2, Genre = "Fantasy, Adventure" });
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 2, Title = "The Two Towers", ReleaseYear = new DateTime(2002, 1, 1), Director = "Peter Jackson", FranchiseId = 2, Genre = "Fantasy, Adventure" });
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 3, Title = "The Return of the King", ReleaseYear = new DateTime(2003, 1, 1), Director = "Peter Jackson", FranchiseId = 2, Genre = "Fantasy, Adventure" });
            
            // Star wars movies
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 4, Title = "Episode IV - A New Hope", ReleaseYear = new DateTime(1977, 5, 25), Director = "George Lukas", FranchiseId = 1, Genre = "Science Fiction" });
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 5, Title = "Episode V - The Empire Strikes Back", ReleaseYear = new DateTime(1980, 5, 21), Director = "Irvin Kershner", FranchiseId = 1, Genre = "Science Fiction" });
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 6, Title = "Episode VI - Return of the Jedi", ReleaseYear = new DateTime(1983, 5, 25), Director = "Richard Marquand", FranchiseId = 1, Genre = "Science Fiction" });
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 7, Title = "Episode I - The Phantom Menace", ReleaseYear = new DateTime(1999, 5, 19), Director = "George Lukas", FranchiseId = 1, Genre = "Science Fiction" });
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 8, Title = "Episode II - Attack of the Clones", ReleaseYear = new DateTime(2002, 5, 16), Director = "George Lukas", FranchiseId = 1, Genre = "Science Fiction" });
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 9, Title = "Episode III - Revenge of the Sith", ReleaseYear = new DateTime(2005, 5, 19), Director = "George Lukas", FranchiseId = 1, Genre = "Science Fiction" });
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 10, Title = "Episode VII - The Force Awakens", ReleaseYear = new DateTime(2015, 12, 18), Director = "J.J. Abrams", FranchiseId = 1, Genre = "Science Fiction" });
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 11, Title = "Episode VII - The Last Jedi", ReleaseYear = new DateTime(2017, 12, 15), Director = "Rian Johnson", FranchiseId = 1, Genre = "Science Fiction" });
            modelBuilder.Entity<Movie>().HasData(new Movie { Id = 12, Title = "Episode IX - The Rise of Skywalker", ReleaseYear = new DateTime(2019, 12,20), Director = "J.J. Abrams", FranchiseId = 1, Genre = "Science Fiction" });

            // Actors 
            // LoTR actors
            modelBuilder.Entity<Actor>().HasData(new Actor() { Id=1, FirstName = "Elijah", LastNames= "Wood", Gender="male", DOB = new DateTime(1981, 1, 28), POB= "Iowa, US"});
            modelBuilder.Entity<Actor>().HasData(new Actor() { Id=2, FirstName = "Sean", OtherNames="Patrick", LastNames= "Astin", Gender="male", DOB = new DateTime(1971, 2, 25), POB= "California, US"});
            modelBuilder.Entity<Actor>().HasData(new Actor() { Id=3, FirstName = "Andy", LastNames= "Serkis", Gender="male", DOB = new DateTime(1964, 4, 20), POB= "Middlesex, England"});
            modelBuilder.Entity<Actor>().HasData(new Actor() { Id=4, FirstName = "Orlando", OtherNames="Jonathan Blanchard Copeland", LastNames= "Bloom", Gender="male", DOB = new DateTime(1977, 1, 13), POB= "Canterbury, England"});
            modelBuilder.Entity<Actor>().HasData(new Actor() { Id=5, FirstName = "John", LastNames= "Rhys-Davies", Gender="male", DOB = new DateTime(1944, 5, 5), POB= "Wiltshire, England"});
            modelBuilder.Entity<Actor>().HasData(new Actor() { Id=6, FirstName = "Ian", OtherNames="Murray", LastNames= "McKellen", Gender="male", DOB = new DateTime(1939, 5, 25), POB= "Lancashire, England"});
            modelBuilder.Entity<Actor>().HasData(new Actor() { Id=7, FirstName = "Viggo", OtherNames="Peter", LastNames= "Mortensen", Gender="male", DOB = new DateTime(1958, 10, 20), POB= "New York, US"});
            modelBuilder.Entity<Actor>().HasData(new Actor() { Id=8, FirstName = "Liv", OtherNames="Rundgren", LastNames= "Tyler", Gender="female", DOB = new DateTime(1977, 7, 1), POB= "New York, US"});

            // characters
            modelBuilder.Entity<Character>().HasData(new Character() { Id = 1, FullName="Frodo Baggins", Alias="Frodo", Gender="male" });
            modelBuilder.Entity<Character>().HasData(new Character() { Id = 2, FullName="Samwise Gamgee", Alias="Sam", Gender="male" });
            modelBuilder.Entity<Character>().HasData(new Character() { Id = 3, FullName="Smeagol", Alias="Gollum", Gender="male" });
            modelBuilder.Entity<Character>().HasData(new Character() { Id = 4, FullName="Legolas Greenleaf", Alias="Legolas", Gender="male" });
            modelBuilder.Entity<Character>().HasData(new Character() { Id = 5, FullName="Gimli son of Gloin", Alias="Gimli", Gender="male" });
            modelBuilder.Entity<Character>().HasData(new Character() { Id = 6, FullName="Galdalf the Grey", Alias="Gandalf", Gender="male" });
            modelBuilder.Entity<Character>().HasData(new Character() { Id = 7, FullName= "Aragorn II Elessar Telcontar", Alias="Aragorn", Gender="male" });
            modelBuilder.Entity<Character>().HasData(new Character() { Id = 8, FullName="Arwen Undomiel", Alias="Arwen", Gender="female" });

            // movie character
            modelBuilder.Entity<MovieCharacter>().HasData( new MovieCharacter() {MovieId = 1, ActorId = 1, CharacterId = 1 });
            modelBuilder.Entity<MovieCharacter>().HasData( new MovieCharacter() {MovieId = 2, ActorId = 1, CharacterId = 1 });
            modelBuilder.Entity<MovieCharacter>().HasData( new MovieCharacter() {MovieId = 3, ActorId = 1, CharacterId = 1 });

            modelBuilder.Entity<MovieCharacter>().HasData( new MovieCharacter() {MovieId = 1, ActorId = 2, CharacterId = 2 });
            modelBuilder.Entity<MovieCharacter>().HasData( new MovieCharacter() {MovieId = 2, ActorId = 2, CharacterId = 2 });
            modelBuilder.Entity<MovieCharacter>().HasData( new MovieCharacter() {MovieId = 3, ActorId = 2, CharacterId = 2 });

            modelBuilder.Entity<MovieCharacter>().HasData( new MovieCharacter() {MovieId = 1, ActorId = 3, CharacterId = 3 });
            modelBuilder.Entity<MovieCharacter>().HasData( new MovieCharacter() {MovieId = 2, ActorId = 3, CharacterId = 3 });
            modelBuilder.Entity<MovieCharacter>().HasData( new MovieCharacter() {MovieId = 3, ActorId = 3, CharacterId = 3 });

            modelBuilder.Entity<MovieCharacter>().HasData( new MovieCharacter() {MovieId = 1, ActorId = 4, CharacterId = 4 });
            modelBuilder.Entity<MovieCharacter>().HasData( new MovieCharacter() {MovieId = 2, ActorId = 4, CharacterId = 4 });
            modelBuilder.Entity<MovieCharacter>().HasData( new MovieCharacter() {MovieId = 3, ActorId = 4, CharacterId = 4 });

            modelBuilder.Entity<MovieCharacter>().HasData( new MovieCharacter() {MovieId = 1, ActorId = 5, CharacterId = 5 });
            modelBuilder.Entity<MovieCharacter>().HasData( new MovieCharacter() {MovieId = 2, ActorId = 5, CharacterId = 5 });
            modelBuilder.Entity<MovieCharacter>().HasData( new MovieCharacter() {MovieId = 3, ActorId = 5, CharacterId = 5 });

            modelBuilder.Entity<MovieCharacter>().HasData( new MovieCharacter() {MovieId = 1, ActorId = 6, CharacterId = 6 });
            modelBuilder.Entity<MovieCharacter>().HasData( new MovieCharacter() {MovieId = 2, ActorId = 6, CharacterId = 6 });
            modelBuilder.Entity<MovieCharacter>().HasData( new MovieCharacter() {MovieId = 3, ActorId = 6, CharacterId = 6 });

            modelBuilder.Entity<MovieCharacter>().HasData( new MovieCharacter() {MovieId = 1, ActorId = 7, CharacterId = 7 });
            modelBuilder.Entity<MovieCharacter>().HasData( new MovieCharacter() {MovieId = 2, ActorId = 7, CharacterId = 7 });
            modelBuilder.Entity<MovieCharacter>().HasData( new MovieCharacter() {MovieId = 3, ActorId = 7, CharacterId = 7 });

            modelBuilder.Entity<MovieCharacter>().HasData( new MovieCharacter() {MovieId = 1, ActorId = 8, CharacterId = 8 });
            modelBuilder.Entity<MovieCharacter>().HasData( new MovieCharacter() {MovieId = 2, ActorId = 8, CharacterId = 8 });
            modelBuilder.Entity<MovieCharacter>().HasData( new MovieCharacter() {MovieId = 3, ActorId = 8, CharacterId = 8 });

        }
    }
}
