﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Text;

namespace MovieCharacterAPI.Models
{
    public class Movie
    {
        // Primary key
        public int Id { get; set; }

        public string Title { get; set; }

        // string comma separated list
        public string Genre { get; set; }

        public DateTime ReleaseYear { get; set; }

        public string Director { get; set; }

        public string PictureURL { get; set; }
        public string TrailerURL { get; set; }

        // navigation/FKs 
        public ICollection<MovieCharacter> Characters { get; set; }

        // FK
        public int FranchiseId { get; set; }

        //[JsonIgnore]
        public Franchise Franchise { get; set; }

    }

}
