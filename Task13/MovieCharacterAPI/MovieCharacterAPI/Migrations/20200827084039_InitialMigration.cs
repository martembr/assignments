﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace MovieCharacterAPI.Migrations
{
    public partial class InitialMigration : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Actor",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FirstName = table.Column<string>(nullable: true),
                    OtherNames = table.Column<string>(nullable: true),
                    LastNames = table.Column<string>(nullable: true),
                    Gender = table.Column<string>(nullable: true),
                    DOB = table.Column<DateTime>(nullable: false),
                    POB = table.Column<string>(nullable: true),
                    Biography = table.Column<string>(nullable: true),
                    PictureURL = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Actor", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Character",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    FullName = table.Column<string>(nullable: true),
                    Alias = table.Column<string>(nullable: true),
                    Gender = table.Column<string>(nullable: true),
                    PictureURL = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Character", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Franchise",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Franchise", x => x.Id);
                });

            migrationBuilder.CreateTable(
                name: "Movie",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Title = table.Column<string>(nullable: true),
                    Genre = table.Column<string>(nullable: true),
                    ReleaseYear = table.Column<DateTime>(nullable: false),
                    Director = table.Column<string>(nullable: true),
                    PictureURL = table.Column<string>(nullable: true),
                    TrailerURL = table.Column<string>(nullable: true),
                    FranchiseId = table.Column<int>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Movie", x => x.Id);
                    table.ForeignKey(
                        name: "FK_Movie_Franchise_FranchiseId",
                        column: x => x.FranchiseId,
                        principalTable: "Franchise",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateTable(
                name: "MovieCharacter",
                columns: table => new
                {
                    MovieId = table.Column<int>(nullable: false),
                    CharacterId = table.Column<int>(nullable: false),
                    ActorId = table.Column<int>(nullable: false),
                    PictureURL = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_MovieCharacter", x => new { x.MovieId, x.CharacterId });
                    table.ForeignKey(
                        name: "FK_MovieCharacter_Actor_ActorId",
                        column: x => x.ActorId,
                        principalTable: "Actor",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MovieCharacter_Character_CharacterId",
                        column: x => x.CharacterId,
                        principalTable: "Character",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                    table.ForeignKey(
                        name: "FK_MovieCharacter_Movie_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movie",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.InsertData(
                table: "Actor",
                columns: new[] { "Id", "Biography", "DOB", "FirstName", "Gender", "LastNames", "OtherNames", "POB", "PictureURL" },
                values: new object[,]
                {
                    { 1, null, new DateTime(1981, 1, 28, 0, 0, 0, 0, DateTimeKind.Unspecified), "Elijah", "male", "Wood", null, "Iowa, US", null },
                    { 2, null, new DateTime(1971, 2, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "Sean", "male", "Astin", "Patrick", "California, US", null },
                    { 3, null, new DateTime(1964, 4, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "Andy", "male", "Serkis", null, "Middlesex, England", null },
                    { 4, null, new DateTime(1977, 1, 13, 0, 0, 0, 0, DateTimeKind.Unspecified), "Orlando", "male", "Bloom", "Jonathan Blanchard Copeland", "Canterbury, England", null },
                    { 5, null, new DateTime(1944, 5, 5, 0, 0, 0, 0, DateTimeKind.Unspecified), "John", "male", "Rhys-Davies", null, "Wiltshire, England", null },
                    { 6, null, new DateTime(1939, 5, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "Ian", "male", "McKellen", "Murray", "Lancashire, England", null },
                    { 7, null, new DateTime(1958, 10, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "Viggo", "male", "Mortensen", "Peter", "New York, US", null },
                    { 8, null, new DateTime(1977, 7, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "Liv", "female", "Tyler", "Rundgren", "New York, US", null }
                });

            migrationBuilder.InsertData(
                table: "Character",
                columns: new[] { "Id", "Alias", "FullName", "Gender", "PictureURL" },
                values: new object[,]
                {
                    { 8, "Arwen", "Arwen Undomiel", "female", null },
                    { 7, "Aragorn", "Aragorn II Elessar Telcontar", "male", null },
                    { 6, "Gandalf", "Galdalf the Grey", "male", null },
                    { 5, "Gimli", "Gimli son of Gloin", "male", null },
                    { 1, "Frodo", "Frodo Baggins", "male", null },
                    { 3, "Gollum", "Smeagol", "male", null },
                    { 2, "Sam", "Samwise Gamgee", "male", null },
                    { 4, "Legolas", "Legolas Greenleaf", "male", null }
                });

            migrationBuilder.InsertData(
                table: "Franchise",
                columns: new[] { "Id", "Description", "Name" },
                values: new object[,]
                {
                    { 1, "In a galaxy far far away ...", "Star Wars" },
                    { 2, "Film adaption of the book of the same name by J.R.R. Tolkien", "The Lord of the Rings" }
                });

            migrationBuilder.InsertData(
                table: "Movie",
                columns: new[] { "Id", "Director", "FranchiseId", "Genre", "PictureURL", "ReleaseYear", "Title", "TrailerURL" },
                values: new object[,]
                {
                    { 4, "George Lukas", 1, "Science Fiction", null, new DateTime(1977, 5, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "Episode IV - A New Hope", null },
                    { 5, "Irvin Kershner", 1, "Science Fiction", null, new DateTime(1980, 5, 21, 0, 0, 0, 0, DateTimeKind.Unspecified), "Episode V - The Empire Strikes Back", null },
                    { 6, "Richard Marquand", 1, "Science Fiction", null, new DateTime(1983, 5, 25, 0, 0, 0, 0, DateTimeKind.Unspecified), "Episode VI - Return of the Jedi", null },
                    { 7, "George Lukas", 1, "Science Fiction", null, new DateTime(1999, 5, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), "Episode I - The Phantom Menace", null },
                    { 8, "George Lukas", 1, "Science Fiction", null, new DateTime(2002, 5, 16, 0, 0, 0, 0, DateTimeKind.Unspecified), "Episode II - Attack of the Clones", null },
                    { 9, "George Lukas", 1, "Science Fiction", null, new DateTime(2005, 5, 19, 0, 0, 0, 0, DateTimeKind.Unspecified), "Episode III - Revenge of the Sith", null },
                    { 10, "J.J. Abrams", 1, "Science Fiction", null, new DateTime(2015, 12, 18, 0, 0, 0, 0, DateTimeKind.Unspecified), "Episode VII - The Force Awakens", null },
                    { 11, "Rian Johnson", 1, "Science Fiction", null, new DateTime(2017, 12, 15, 0, 0, 0, 0, DateTimeKind.Unspecified), "Episode VII - The Last Jedi", null },
                    { 12, "J.J. Abrams", 1, "Science Fiction", null, new DateTime(2019, 12, 20, 0, 0, 0, 0, DateTimeKind.Unspecified), "Episode IX - The Rise of Skywalker", null },
                    { 1, "Peter Jackson", 2, "Fantasy, Adventure", null, new DateTime(2001, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "The Fellowship of the Ring", null },
                    { 2, "Peter Jackson", 2, "Fantasy, Adventure", null, new DateTime(2002, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "The Two Towers", null },
                    { 3, "Peter Jackson", 2, "Fantasy, Adventure", null, new DateTime(2003, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified), "The Return of the King", null }
                });

            migrationBuilder.InsertData(
                table: "MovieCharacter",
                columns: new[] { "MovieId", "CharacterId", "ActorId", "PictureURL" },
                values: new object[,]
                {
                    { 1, 1, 1, null },
                    { 3, 6, 6, null },
                    { 3, 5, 5, null },
                    { 3, 4, 4, null },
                    { 3, 3, 3, null },
                    { 3, 2, 2, null },
                    { 3, 1, 1, null },
                    { 2, 8, 8, null },
                    { 2, 7, 7, null },
                    { 2, 6, 6, null },
                    { 2, 5, 5, null },
                    { 2, 4, 4, null },
                    { 2, 3, 3, null },
                    { 2, 2, 2, null },
                    { 2, 1, 1, null },
                    { 1, 8, 8, null },
                    { 1, 7, 7, null },
                    { 1, 6, 6, null },
                    { 1, 5, 5, null },
                    { 1, 4, 4, null },
                    { 1, 3, 3, null },
                    { 1, 2, 2, null },
                    { 3, 7, 7, null },
                    { 3, 8, 8, null }
                });

            migrationBuilder.CreateIndex(
                name: "IX_Movie_FranchiseId",
                table: "Movie",
                column: "FranchiseId");

            migrationBuilder.CreateIndex(
                name: "IX_MovieCharacter_ActorId",
                table: "MovieCharacter",
                column: "ActorId");

            migrationBuilder.CreateIndex(
                name: "IX_MovieCharacter_CharacterId",
                table: "MovieCharacter",
                column: "CharacterId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "MovieCharacter");

            migrationBuilder.DropTable(
                name: "Actor");

            migrationBuilder.DropTable(
                name: "Character");

            migrationBuilder.DropTable(
                name: "Movie");

            migrationBuilder.DropTable(
                name: "Franchise");
        }
    }
}
