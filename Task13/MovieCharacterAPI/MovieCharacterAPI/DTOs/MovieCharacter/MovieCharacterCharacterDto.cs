﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.DTOs.MovieCharacter
{
    public class MovieCharacterCharacterDto
    {
        public int MovieId { get; set; }
        public int ActorId { get; set; }

        public string Character { get; set; }
    }
}
