﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.DTOs.MovieCharacter
{
    public class MovieCharacterMovieDto
    {
        public string Movie { get; set; }
        public int ActorId { get; set; }

        public int CharacterId { get; set; }
    }
}
