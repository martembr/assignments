﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.DTOs.MovieCharacter
{
    public class MovieCharacterActorDto
    {
        public int MovieId { get; set; }
        public string Actor { get; set; }

        public int CharacterId { get; set; }
    }
}
