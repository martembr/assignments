﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.DTOs.MovieCharacter
{
    public class MovieCharacterListDto
    {
        public int MovieId { get; set; }
        public int ActorId { get; set; }

        public int CharacterId { get; set; }
        public string Movie { get; set; }
        public string Actor { get; set; }
        public string Character { get; set; }
        
    }
}
