﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.DTOs.Character
{
    public class CharacterDto
    {
        public int Id { get; set; }

        public string FullName { get; set; }
        public string Alias { get; set; } // optional
        public string Gender { get; set; }
        public string PictureURL { get; set; }

    }
}
