﻿using MovieCharacterAPI.DTOs.Actor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.DTOs.Character
{
    public class CharacterActorsDto
    {
        public int Id { get; set; }

        public string FullName { get; set; }

        public ICollection<ActorListDto> actors { get; set; }
    }
}
