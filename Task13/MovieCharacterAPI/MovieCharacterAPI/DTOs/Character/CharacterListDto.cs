﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.DTOs.Character
{
    public class CharacterListDto
    {
        public int Id { get; set; }

        public string FullName { get; set; }

    }
}
