﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.DTOs.Movie
{
    public class MovieDto
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Genre { get; set; }

        public DateTime ReleaseYear { get; set; }

        public string Director { get; set; }

        public string PictureURL { get; set; }
        public string TrailerURL { get; set; }

        public int FranchiseId { get; set; }

    }
}
