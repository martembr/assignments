﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.DTOs.Actor
{
    public class ActorListDto
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        // other names optional
        public string OtherNames { get; set; }
        public string LastNames { get; set; }
    }
}
