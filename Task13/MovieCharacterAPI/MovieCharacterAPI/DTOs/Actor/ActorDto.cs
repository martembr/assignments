﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.DTOs.Actor
{
    public class ActorDto
    {
        public int Id { get; set; }

        public string FirstName { get; set; }

        // other names optional
        public string OtherNames { get; set; }
        public string LastNames { get; set; }

        public string Gender { get; set; }
        public DateTime DOB { get; set; }

        public string POB { get; set; }

        public string Biography { get; set; }
        public string PictureURL { get; set; }
    }
}
