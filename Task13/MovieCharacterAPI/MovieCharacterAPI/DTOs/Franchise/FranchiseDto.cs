﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MovieCharacterAPI.DTOs.Franchise
{
    public class FranchiseDto
    {
        public int Id { get; set; }

        public String Name { get; set; }
        public String Description { get; set; }
    }
}
