﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.DTOs.Actor;
using MovieCharacterAPI.DTOs.Character;
using MovieCharacterAPI.DTOs.MovieCharacter;
using MovieCharacterAPI.Models;

namespace MovieCharacterAPI.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class CharactersController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;

        public CharactersController(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/v1/Characters
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CharacterDto>>> GetCharacter()
        {
            //return await _context.Character.ToListAsync();
            var characters = await _context.Character.ToListAsync();
            if (characters == null)
            {
                return NotFound();
            }
            List<CharacterDto> characterDtos = _mapper.Map<List<CharacterDto>>(characters);
            return Ok(characterDtos);
        }

        // GET: api/v1/Characters/5
        [HttpGet("{id}")]
        public async Task<ActionResult<CharacterDto>> GetCharacter(int id)
        {
            var character = await _context.Character.FindAsync(id);

            if (character == null)
            {
                return NotFound();
            }
            CharacterDto charDto = _mapper.Map<CharacterDto>(character);
            return Ok(charDto);
        }


        // GET: api/v1/Characters/5/Actors
        [HttpGet("{id}/Actors")]
        public async Task<ActionResult<IEnumerable<MovieCharacterActorDto>>> GetCharacterActors(int id)
        {

            var character = await _context.Character.Include(c => c.Moviecharacters)
                .ThenInclude(mc => mc.Actor)
                .Where(c => c.Id == id).SingleOrDefaultAsync();
            if (character == null)
            {
                return NotFound();
            }
            IEnumerable<MovieCharacter> mcharacters = character.Moviecharacters.ToList();

            return Ok(_mapper.Map<List<MovieCharacterActorDto>>(mcharacters));
        }


        // PUT: api/v1/Characters/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCharacter(int id, Character character)
        {
            if (id != character.Id)
            {
                return BadRequest();
            }

            _context.Entry(character).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/v1/Characters
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Character>> PostCharacter(Character character)
        {
            _context.Character.Add(character);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCharacter", new { id = character.Id }, character);
        }

        // DELETE: api/v1/Characters/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Character>> DeleteCharacter(int id)
        {
            var character = await _context.Character.FindAsync(id);
            if (character == null)
            {
                return NotFound();
            }

            _context.Character.Remove(character);
            await _context.SaveChangesAsync();

            return character;
        }

        private bool CharacterExists(int id)
        {
            return _context.Character.Any(e => e.Id == id);
        }
    }
}
