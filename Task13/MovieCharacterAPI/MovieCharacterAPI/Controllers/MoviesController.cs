﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.DTOs.Movie;
using MovieCharacterAPI.DTOs.MovieCharacter;
using MovieCharacterAPI.Models;

namespace MovieCharacterAPI.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class MoviesController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;

        public MoviesController(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/v1/Movies
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieDto>>> GetMovie()
        {
            //return await _context.Movie.ToListAsync();
            var movies = await _context.Movie.ToListAsync();
            if (movies == null)
            {
                return NotFound();
            }
            return Ok(_mapper.Map<List<MovieDto>>(movies));
        }

        // GET: api/v1/Movies/5
        [HttpGet("{id}")]
        public async Task<ActionResult<MovieDto>> GetMovie(int id)
        {
            var movie = await _context.Movie.FindAsync(id);

            if (movie == null)
            {
                return NotFound();
            }
            MovieDto movieDto = _mapper.Map<MovieDto>(movie);
            return Ok(movieDto);
        }

 
        // GET: api/v1/Movies/5/Characters
        [HttpGet("{id}/Characters")]
        public async Task<ActionResult<IEnumerable<MovieCharacterListDto>>> GetMovieCharacters(int id)
        {
            
            var movie = await _context.Movie.Include(m => m.Characters)
                .ThenInclude(mc => mc.Character).ThenInclude(c => c.Moviecharacters)
                .ThenInclude(mc => mc.Actor).Where(m => m.Id == id).SingleOrDefaultAsync();
            if (movie == null)
            {
                return NotFound();
            }
            IEnumerable<MovieCharacter> characters = movie.Characters.ToList();
            return Ok(_mapper.Map<List<MovieCharacterListDto>>(characters));
        }

        // PUT: api/v1/Movies/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovie(int id, Movie movie)
        {
            if (id != movie.Id)
            {
                return BadRequest();
            }

            _context.Entry(movie).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/v1/Movies
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Movie>> PostMovie(Movie movie)
        {
            _context.Movie.Add(movie);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetMovie", new { id = movie.Id }, movie);
        }

        // DELETE: api/v1/Movies/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Movie>> DeleteMovie(int id)
        {
            var movie = await _context.Movie.FindAsync(id);
            if (movie == null)
            {
                return NotFound();
            }

            _context.Movie.Remove(movie);
            await _context.SaveChangesAsync();

            return movie;
        }

        private bool MovieExists(int id)
        {
            return _context.Movie.Any(e => e.Id == id);
        }
    }
}
