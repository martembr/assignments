﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.DTOs.MovieCharacter;
using MovieCharacterAPI.Models;

namespace MovieCharacterAPI.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class MovieCharactersController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;

        public MovieCharactersController(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

    
        // GET: api/v1/MovieCharacters 
        [HttpGet]
        public async Task<ActionResult<IEnumerable<MovieCharacterListDto>>> GetMovieCharacter()
        {
            var mcharacters = await _context.MovieCharacter.Include(mc => mc.Actor)
                .Include(mc => mc.Character).Include(mc => mc.Movie)
                .ToListAsync();
            return _mapper.Map<List<MovieCharacterListDto>>(mcharacters);
        }

        // does not work --- 404 not found
        // GET: api/v1/MovieCharacters/5/4
        [HttpGet("{mid}/{cid}")]
        public async Task<ActionResult<MovieCharacterListDto>> GetMovieCharacter(int mid, int cid)
        {
            var movieCharacter = await _context.MovieCharacter.Include(mc => mc.Actor)
                .Include(mc => mc.Character).Include(mc => mc.Movie)
                .Where(mc => mc.MovieId == mid && mc.CharacterId == cid).SingleOrDefaultAsync();

            if (movieCharacter == null)
            {
                return NotFound();
            }

            return Ok(_mapper.Map<MovieCharacterListDto>(movieCharacter));
        }

        // PUT: api/v1/MovieCharacters/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutMovieCharacter(int id, MovieCharacter movieCharacter)
        {
            if (id != movieCharacter.MovieId)
            {
                return BadRequest();
            }

            _context.Entry(movieCharacter).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!MovieCharacterExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/v1/MovieCharacters
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<MovieCharacter>> PostMovieCharacter(MovieCharacter movieCharacter)
        {
            _context.MovieCharacter.Add(movieCharacter);
            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateException)
            {
                if (MovieCharacterExists(movieCharacter.MovieId))
                {
                    return Conflict();
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtAction("GetMovieCharacter", new { id = movieCharacter.MovieId }, movieCharacter);
        }

        // DELETE: api/v1/MovieCharacters/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<MovieCharacter>> DeleteMovieCharacter(int id)
        {
            var movieCharacter = await _context.MovieCharacter.FindAsync(id);
            if (movieCharacter == null)
            {
                return NotFound();
            }

            _context.MovieCharacter.Remove(movieCharacter);
            await _context.SaveChangesAsync();

            return movieCharacter;
        }

        private bool MovieCharacterExists(int id)
        {
            return _context.MovieCharacter.Any(e => e.MovieId == id);
        }
    }
}
