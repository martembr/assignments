﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.DTOs.Character;
using MovieCharacterAPI.DTOs.Franchise;
using MovieCharacterAPI.DTOs.Movie;
using MovieCharacterAPI.DTOs.MovieCharacter;
using MovieCharacterAPI.Models;

namespace MovieCharacterAPI.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class FranchisesController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;
        // add mapper and change to DTOs, FranchiseDto, FranchiseListDto, MovieListDto


        public FranchisesController(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/v1/Franchises
        [HttpGet]
        public async Task<ActionResult<IEnumerable<FranchiseDto>>> GetFranchise()
        {
            List<Franchise> franchises = await _context.Franchise.ToListAsync();
            if (franchises == null)
            {
                return NotFound();
            }
            List<FranchiseDto> franchisesDto = _mapper.Map<List<FranchiseDto>>(franchises);
            return Ok(franchisesDto);
        }

        // GET: api/v1/Franchises/5
        [HttpGet("{id}")]
        public async Task<ActionResult<FranchiseDto>> GetFranchise(int id)
        {
            var franchise = await _context.Franchise.FindAsync(id);

            if (franchise == null)
            {
                return NotFound();
            }
            FranchiseDto franchiseDto = _mapper.Map<FranchiseDto>(franchise);
            return Ok(franchiseDto);
        }

        // GET: api/v1/Franchises/5/Movies
        [HttpGet("{id}/Movies")]
        public async Task<ActionResult<IEnumerable<MovieFranchiseDto>>> GetFranchiseMovies(int id)
        {
            var franchise = await _context.Franchise.Include(f => f.Movies).Where(f => f.Id == id).SingleOrDefaultAsync();
            if (franchise == null)
            {
                return NotFound();
            }
            IEnumerable<Movie> movies = franchise.Movies.ToList();

            List<MovieFranchiseDto> moviesdto = _mapper.Map<List<MovieFranchiseDto>>(movies);
            
            return Ok(moviesdto);
        }

     
        // GET: api/v1/Franchises/5/Characters
        [HttpGet("{id}/Characters")]
        public async Task<ActionResult<IEnumerable<CharacterListDto>>> GetFranchiseCharacters(int id)
        {
            var franchise = await _context.Franchise.Include(f => f.Movies).ThenInclude(m => m.Characters)
                .ThenInclude(mc => mc.Character).Where(f => f.Id == id).SingleOrDefaultAsync();
            
            if (franchise == null)
            {
                return NotFound();
            }
            IEnumerable<Movie> movies = franchise.Movies.ToList();
            List<Character> characters = new List<Character>();
            foreach(Movie m in movies)
            {
                foreach(MovieCharacter mc in m.Characters)
                {
                    characters.Add(mc.Character);
                }
            }
            return Ok(_mapper.Map<List<CharacterListDto>>(characters));
        }

        // PUT: api/v1/Franchises/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutFranchise(int id, Franchise franchise)
        {
            if (id != franchise.Id)
            {
                return BadRequest();
            }

            _context.Entry(franchise).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!FranchiseExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/v1/Franchises
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Franchise>> PostFranchise(Franchise franchise)
        {
            _context.Franchise.Add(franchise);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetFranchise", new { id = franchise.Id }, franchise);
        }

        // DELETE: api/v1(Franchises/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Franchise>> DeleteFranchise(int id)
        {
            var franchise = await _context.Franchise.FindAsync(id);
            if (franchise == null)
            {
                return NotFound();
            }

            _context.Franchise.Remove(franchise);
            await _context.SaveChangesAsync();

            return franchise;
        }

        private bool FranchiseExists(int id)
        {
            return _context.Franchise.Any(e => e.Id == id);
        }
    }
}
