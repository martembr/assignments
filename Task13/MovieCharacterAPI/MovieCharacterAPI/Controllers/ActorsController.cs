﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using MovieCharacterAPI.DTOs.Actor;
using MovieCharacterAPI.DTOs.Character;
using MovieCharacterAPI.DTOs.Movie;
using MovieCharacterAPI.DTOs.MovieCharacter;
using MovieCharacterAPI.Models;

namespace MovieCharacterAPI.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class ActorsController : ControllerBase
    {
        private readonly MovieCharacterDbContext _context;
        private readonly IMapper _mapper;

        public ActorsController(MovieCharacterDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        // GET: api/v1/Actors
        [HttpGet]
        public async Task<ActionResult<IEnumerable<ActorDto>>> GetActor()
        {
            //return await _context.Actor.ToListAsync();
            var actors = await _context.Actor.ToListAsync();
            if (actors == null)
            {
                return NotFound();
            }
            List<ActorDto> actorsDto = _mapper.Map<List<ActorDto>>(actors);
            return Ok(actorsDto);
        }

        // GET: api/v1/Actors/5
        [HttpGet("{id}")]
        public async Task<ActionResult<ActorDto>> GetActor(int id)
        {
            var actor = await _context.Actor.FindAsync(id);

            if (actor == null)
            {
                return NotFound();
            }
            // map to ActorDto
            ActorDto actordto = _mapper.Map<ActorDto>(actor);

            return Ok(actordto);
        }



        // GET: api/v1/Actors/5/Characters
        [HttpGet("{id}/Characters")]
        public async Task<ActionResult<IEnumerable<MovieCharacterListDto>>> GetActorCharacters(int id)
        {

            var actor = await _context.Actor.Include(a => a.MoviecharactersPlayed)
                .ThenInclude(mc => mc.Character).ThenInclude(c => c.Moviecharacters).ThenInclude(mc => mc.Movie)
                .Where(a => a.Id == id).SingleOrDefaultAsync();
            if (actor == null)
            {
                return NotFound();
            }
            IEnumerable<MovieCharacter> mcharacters = actor.MoviecharactersPlayed.ToList();

            return Ok(_mapper.Map<List<MovieCharacterListDto>>(mcharacters));
        }

        // GET: api/v1/Characters/5/Movies
        [HttpGet("{id}/Movies")]
        public async Task<ActionResult<IEnumerable<MovieListDto>>> GetActorMovies(int id)
        {
            var actor = await _context.Actor.Include(a => a.MoviecharactersPlayed)
                .ThenInclude(mc => mc.Movie).Where(a => a.Id == id).SingleOrDefaultAsync();
            if (actor == null)
            {
                return NotFound();
            }
            IEnumerable<MovieCharacter> mcharacters = actor.MoviecharactersPlayed.ToList();
            List<Movie> movies = new List<Movie>();
            foreach (MovieCharacter mc in mcharacters)
            {
                movies.Add(mc.Movie);
            }

            return Ok(_mapper.Map<List<MovieListDto>>(movies));
        }

        // PUT: api/v1/Actors/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPut("{id}")]
        public async Task<IActionResult> PutActor(int id, Actor actor)
        {
            if (id != actor.Id)
            {
                return BadRequest();
            }

            _context.Entry(actor).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ActorExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/v1/Actors
        // To protect from overposting attacks, enable the specific properties you want to bind to, for
        // more details, see https://go.microsoft.com/fwlink/?linkid=2123754.
        [HttpPost]
        public async Task<ActionResult<Actor>> PostActor(Actor actor)
        {
            _context.Actor.Add(actor);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetActor", new { id = actor.Id }, actor);
        }

        // DELETE: api/v1/Actors/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Actor>> DeleteActor(int id)
        {
            var actor = await _context.Actor.FindAsync(id);
            if (actor == null)
            {
                return NotFound();
            }

            _context.Actor.Remove(actor);
            await _context.SaveChangesAsync();

            return actor;
        }

        private bool ActorExists(int id)
        {
            return _context.Actor.Any(e => e.Id == id);
        }
    }
}
