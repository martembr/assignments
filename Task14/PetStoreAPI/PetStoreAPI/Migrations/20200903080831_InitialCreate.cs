﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace PetStoreAPI.Migrations
{
    public partial class InitialCreate : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Pet",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    Name = table.Column<string>(nullable: true),
                    Category = table.Column<string>(nullable: true),
                    Breed = table.Column<string>(nullable: true),
                    Price = table.Column<double>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Pet", x => x.Id);
                });

            migrationBuilder.InsertData(
                table: "Pet",
                columns: new[] { "Id", "Breed", "Category", "Name", "Price" },
                values: new object[,]
                {
                    { 1, "Persian cat", "Cats", "Fluffy", 3050.9000000000001 },
                    { 2, "Golden Retriever", "Dogs", "Fido", 4050.9000000000001 },
                    { 3, "Bulldog", "Dogs", "Hank", 3550.9000000000001 },
                    { 4, "Tabby cat", "Cats", "Mao", 3050.9000000000001 },
                    { 5, "King rabbit", "Small pets", "Priscilla", 550.89999999999998 },
                    { 6, "Guinnea Pig", "Small pets", "Kate", 450.89999999999998 },
                    { 7, "Parrot", "Birds", "Hector", 350.89999999999998 },
                    { 8, "Budgie", "Birds", "Victoria", 250.90000000000001 }
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Pet");
        }
    }
}
