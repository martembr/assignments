﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PetStoreAPI.Models
{
    public class PetStoreDbContext : DbContext
    {
        // override constructor
        public PetStoreDbContext(DbContextOptions builder) : base(builder) { }

        // DbSets
        public DbSet<Pet> Pet { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // seed with data

            // Pets
            modelBuilder.Entity<Pet>().HasData(new Pet { Id = 1, Name = "Fluffy", Category = "Cats", Breed = "Persian cat", Price = 3050.9 });
            modelBuilder.Entity<Pet>().HasData(new Pet { Id = 2, Name = "Fido", Category = "Dogs", Breed = "Golden Retriever", Price = 4050.9 });
            modelBuilder.Entity<Pet>().HasData(new Pet { Id = 3, Name = "Hank", Category = "Dogs", Breed = "Bulldog", Price = 3550.9 });
            modelBuilder.Entity<Pet>().HasData(new Pet { Id = 4, Name = "Mao", Category = "Cats", Breed = "Tabby cat", Price = 3050.9 });
            modelBuilder.Entity<Pet>().HasData(new Pet { Id = 5, Name = "Priscilla", Category = "Small pets", Breed = "King rabbit", Price = 550.9 });
            modelBuilder.Entity<Pet>().HasData(new Pet { Id = 6, Name = "Kate", Category = "Small pets", Breed = "Guinnea Pig", Price = 450.9 });
            modelBuilder.Entity<Pet>().HasData(new Pet { Id = 7, Name = "Hector", Category = "Birds", Breed = "Parrot", Price = 350.9 });
            modelBuilder.Entity<Pet>().HasData(new Pet { Id = 8, Name = "Victoria", Category = "Birds", Breed = "Budgie", Price = 250.9 });

        }
    }
}
